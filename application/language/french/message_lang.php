<?php
/*Web general*/
$lang['home']='Accueil';
$lang['aboutus']='A Propos';
$lang['services']='Services';
$lang['quote']='Devis en ligne';

/*Nav*/
$lang['projects']='Réalisations';
$lang['contactus']='Contact';
$lang['slider1']='We provide the highest quality in web design';
$lang['slider2']= 'and website development';
$lang['pslider']='Web Design, Corporate Branding, E-Marketing Services and Search Engines, Customizing Websites for Search Engine Standards ...';

/*Constants for Intro*/
$lang['content1']='
Nous travaillons dur pour lancer un bouquet de produits numériques de première qualité';
$lang['pcontent1']='Devis en ligne';
$lang['content2']='Notre mission est de fournir les meilleures solutions e-business et services professionnels, spécialement conçus en fonction des besoins des entreprises';

/*Constants for Firt section*/
$lang['pcontent2']='Nous adaptons les meilleures technologies pour une expérience utilisateur optimale en développement des solutions web sur mesure innovantes pour répondre à vos exigences de productivité et de performance';
$lang['content3']='
SOFTIFI vous accompagne dans la réussite de votre activité en analysant les performances de votre présence en ligne. 
Elle vous portera des meilleures pratiques et des stratégies digitales pertinentes afin d’atteindre vos objectifs.
';
$lang['pcontent3']='You have special products, but you can t convince your customers how distinctive your product is? Can customers reach you but no one buys?';

/*Constants for Second section*/
$lang['taboutus']='Softifi est une agence de création de site internet qui est spécialisée dans la création de site web.Notre agence web en Tunisie met à votre disposition une équipe d’experts et de développeur web au service de votre performance.';
$lang['t1']='Technical skill';
    /*Constants sub*/
    $lang['t2']='Efficiency and professionalism';
    $lang['t3']='Ensure customer satisfaction';
    $lang['Happyclients']='Happy Clients';
    $lang['success']='Success';
    $lang['tservices']='Websites and Mobile applications designed specifically to your request';
$lang['s1']='Tous';

/*Constants for Get Started*/
$lang['s2']='Applications Mobiles';
$lang['s3']='Developpement web';
$lang['s4']='E-commerce';
$lang['s5']='Digital Marketing';

/*Constants for Footer*/
$lang['s6']='Web Hosting';
$lang['des1']='ERP systems are of great importance in managing thousands of companies of all sizes in all fields';
$lang['pdes1']='
Our belief in creating tangible value for our customers Advanced Technologies works with you step-by-step to prove your company vision] business identity and technological goals.';

$lang['ades2']='An integrated management system, offers a range of business applications that help to manage corporate projects of all sizes.';
$lang['ades3']='Create an attractive and easy-to-use design that helps you grow your business in a seamless way where the user visiting your website can achieve what he needs.';

$lang['des2']='A leading enterprise specializing in web design, e-commerce';

$lang['ades4']='
Development of smart phone applications
';

$lang['ades5']='E-commerce solutions';
$lang['ades6']='Web Development';
$lang['process']='Plus';
$lang['p1']='Planning';
$lang['pp1']='Establishing of communication with the client helps to identify the objectives and initial steps of site design.';
$lang['p2']='Design and development';
$lang['pp2']='It is very important to ensure that all services of the site operate efficiently from the content of the site, the speed of the download, the compatibility of the site with mobile devices';
$lang['c3']='Des clients satisfaits';
$lang['c4']='Appel professionnel';
$lang['c1']= 'Ligne de code';
$lang['c2'] ='Travail accompli
';
$lang['name']='Nom';
$lang['email']='Adresse Email';
$lang['phone']='Numero de Telephone';
$lang['message']='Votre Message....';
$lang['sujet']='Sujet';

$lang['send']='Envoyer';
$lang['pnnewsletter']='Suivez l actualité et les offres
';
$lang['newsletter']='Newsletter';

$lang['title_contact'] = 'ILS NOUS ONT FAIT CONFIANCE

';
$lang['pnewsletter'] = 'Suivez l actualité et les offres


';


$lang['button']='Contactez Nous';

$lang['english']='English';
$lang['arabic']='عربي';
$lang['lien']='Lien utiles';
$lang['pname']='Partner Name ';
$lang['function']='Function';
$lang['empnumber']='Employees Number';
?>
