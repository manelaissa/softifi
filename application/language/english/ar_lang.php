<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['SenderName']			= 'SenderName';
$lang['contactSubject']			= 'contactSubject';
$lang['contactPhone']			= 'contactPhone';
$lang['contactMail']			= 'contactMail';
$lang['Delete']			= 'Delete';
$lang['Action']			= 'Action';
$lang['Contacts']			= 'Contacts';
$lang['Services']			= 'Services';
$lang['Social Media']			= 'Social Media';
$lang['Products']			= 'Products';
$lang['Our Products']			= 'Our Products';
$lang['Contact Form']			= 'Contact Form';
$lang['Follow Us:']			= 'Follow Us:';
$lang['Who We Are??']			= 'Who We Are??';
$lang['important links']			= 'important links';
$lang['Our Informations']			= 'Our Informations';
$lang['ServicePicture']			= 'ServicePicture';
$lang['ServiceName']			= 'ServiceName';
$lang['ServiceDescription']			= 'ServiceDescription';
$lang['ServiceDate']			= 'ServiceDate';
$lang['Our Team']			= 'Our Team';
$lang['Create New Team']			= 'Create New Team';
$lang['Open Sources Solutions']			= 'Open Sources Solutions';
$lang['Recent News']			= 'Recent News';

$lang['medical technology']			= 'medical technology';
$lang['Technology']			= 'Technology';
$lang['Product Type:']			= 'Product Type:';

$lang['technology']			= 'technology';

$lang['Offers']			= 'Offers';
$lang['All']			= 'All';
$lang['Portfolio']			= 'Portfolio';


$lang['show']			= 'show';
$lang['Edit']			= 'Edit';
$lang['Create New Product']			= 'Create New Product';

$lang['Title']			= 'Title';
$lang['Description']			= 'Description';
$lang['Picture']			= 'Picture';
$lang['DateProduct']			= 'DateProduct';

$lang['Back']			= 'Back';
$lang['Add Product Name:'] = 'Add Product Name:';
$lang['Add Product Arabic Name:'] = 'Add Product Arabic Name:';
$lang['Add'] = 'Add';
$lang['Add Description']= 'Add Description';
$lang['Add arabic Description']= 'Add arabic Description';

$lang['TitleSM']			= 'Title Social-Media';
$lang['DescriptionSM']			= 'Description Social-Media';
$lang['PictureSM']			= 'Picture Social-Media';
$lang['DateSM']			= 'Date Social-Media';

$lang['Create New SocialMedia']			= 'Create New SocialMedia';

$lang['English Name:']= 'English Name:';
$lang['Arabic Name:']= 'Arabic Name:';

$lang['Create New Service']			= 'Create New Service';
$lang['Contacts']			= 'Contacts';

$lang['About us']			= 'About us';

$lang['Advanced Technology Company']			= 'Advanced Technology Company';
$lang['success is based on a simple common philosophy that is geared to timely processing of customer products with the highest quality.']			= 'success is based on a simple common philosophy that is geared to timely processing of customer products with the highest quality.';

$lang['Media Icon:']			= 'Media Icon:';
$lang['contactMessage']			= 'contactMessage';


$lang['facebook']			= 'facebook';
$lang['twitter']			= 'twitter';
$lang['google-plus']			= 'google-plus';
$lang['linkedin']			= 'linkedin';


$lang['Connect with us']			= 'Connect with us';
$lang['Arabie Saudite']			= 'Arabie Saudite';
$lang['Saudite,9584']			= 'Saudite,9584';
$lang['Our Services']			= 'Our Services';      
$lang['About Us']			= 'About Us';


$lang['Full Name']			= 'Full Name';
$lang['Email Address']			= 'Email Address';      
$lang['Phone Number']			= 'Phone Number';
$lang['Full Name']			= 'Full Name';
$lang['Subject']			= 'Subject';      
$lang['Your Message']			= 'Your Message';
$lang['Full Name']			= 'Full Name';

$lang['Send Message']			= 'Send Message';
$lang['Click here to submit your message!']			= 'Click here to submit your message!'; 




$lang['Our Quick Contact From']			= 'Our Quick Contact From';
$lang['please fill out the form below!']			= 'please fill out the form below!';


$lang['Arabie Saudite,KSA']			= 'Arabie Saudite,KSA';
$lang['Welcome To Advanced Technologie Campany !']			= 'Welcome To Advanced Technologie Campany !';

$lang['ATIT is a company specialized in providing excellent technical services through the design of systems, programs and applications and supporting them, and providing the necessary consultancy, by professional competencies, to achieve speed in the work of institutions and its accuracy and ease in all sectors.']			= 'ATIT is a company specialized in providing excellent technical services through the design of systems, programs and applications and supporting them, and providing the necessary consultancy, by professional competencies, to achieve speed in the work of institutions and its accuracy and ease in all sectors.';


$lang['About']			= 'About';

$lang['Description1']			= 'Description1';
$lang['Description2']			= 'Description2';
$lang['First Property']			= 'First Property';
$lang['Description Property1']			= 'Description Property1';
$lang['Second Property']			= 'Second Property';
$lang['Description Property2']			= 'Description Property2';
$lang['Third Property']			= 'Third Property';
$lang['Description Property3']			= 'Description Property3';
$lang['CharactIcon1']			= 'Property icon1';
$lang['CharactIcon2']			= 'Property icon2';
$lang['CharactIcon3']			= 'Property icon3';
$lang['Add Time']			= 'Add Time';

$lang['cogs']			= 'cogs';
$lang['wrench']			= 'wrench';
$lang['history']			= 'history';
$lang['line-chart']			= 'line-chart';


$lang['Description1Ar']			= 'Description1Ar';
$lang['Description2Ar']			= 'Description2Ar';
$lang['First PropertyAr']			= 'First PropertyAr';
$lang['Description Property1Ar']			= 'Description Property1Ar';
$lang['Second PropertyAr']			= 'Second PropertyAr';
$lang['Description Property2Ar']			= 'Description Property2Ar';
$lang['Third PropertyAr']			= 'Third PropertyAr';
$lang['Description Property3Ar']			= 'Description Property3Ar';









$lang['my_account']			= 'My Account';

$lang['My Profile']	= 'My Profile';
$lang['Log out']= 'Log out';
$lang['ar']			= 'Ar';
$lang['- Product - Detail_prod']='- Product - Detail_prod';
$lang['Member since']='Member since';
$lang['header']			= 'Sell or Advertise anything online with Resale ';
$lang['There are no Posts']='There are no Posts';
$lang['head']= 'sssssssssssssssssss ';
$lang['Home']='Home';
$lang['All Ads']='All Ads';
$lang['Added at']='Added at';
$lang['owner_post']='Owner post';
$lang['Ad ID:']='Ad ID:';
$lang['Brand']='Brand';
$lang['Views']='Views';
$lang['Price']='Price:';
$lang['Add to card']='Add to card:';
$lang['Rating']='Rating:';
$lang['Condition']='Condition';
$lang['Type:']='Type:';
$lang['Posted']='Posted';
$lang['Ads']='Ads';
$lang['Sell or Advertise']='Sell or Advertise';
$lang['Lorem Ipsum is simply dummy text of the printing and typesetting industry']='Sell or Advertise anything online with Resale';
$lang['Post Free Ad']='Post Free Ad';
$lang['anything online']='anything online';

$lang['Member Since']='Member Since';
$lang['Contact the Seller']='Contact the Seller';
$lang['Add Evaluator']='Add Evaluator';
// $lang['Contact the Seller']='Contact the Seller';
$lang['Description']='Description';
$lang['post_free_ad']			= 'Post Free Ad';
$lang['Select Category']= 'Select Category';
$lang['Lorem ipsum dolor sit amet consectetur adipisicing.']= 'Lorem ipsum dolor sit amet consectetur adipisicing.';
$lang['Post']= 'Post';
$lang['By clicking post Button you accept our Terms of Use and Privacy Policy']= 'By clicking post Button you accept our Terms of Use and Privacy Policy';
$lang['Select File']= 'Select File';
$lang['Picture For Your Ad :']= 'Picture For Your Ad :';

$lang['Ad Price:']= 'Ad Price:';
$lang['Type Post']= 'Type Post';
$lang['Needs']= 'Needs';
$lang['Deals']= 'Deals';
$lang['trending_ads']			= 'Trending Ads';
$lang['category']			= 'category';
$lang['All_Ads']			= 'All Ads';
$lang['home']			= 'Home';
$lang['Home']= 'Home';
$lang['main_categories']			= 'Main Categories';
$lang['all_ksa']			= 'All KSA';
$lang['select_city']			= 'Select your city to see local ads';
$lang['all_categories']			= 'All Categories';
$lang['all_posts']			= ' all posts';
$lang['all_needs']          = 'All Needs';
$lang['search_product']			= 'Search for a specific product';
$lang['view']			= 'view';
$lang['sort']			= 'Sort By';
$lang['Dont have an account?'] = 'Dont have an account?';
//$lang['Sign up'] = 'اتبعنا';
$lang['Forgot your password?'] = 'Forgot your password?';
$lang['recent']			= 'Most recent';
$lang['old']			= 'The Oldest';
$lang['low_high']			= 'Price: Rs Low to High';
$lang['high_low']			= 'Price: Rs High to Low';
$lang['no_posts']			= 'There are no Posts ';
$lang['Add Evaluation Request'] = 'Add Evaluation Request ';
$lang['Product description']= 'Product description ';
$lang['Contact the Evaluator']= 'Contact the Evaluator ';
$lang['Publication']= 'Publication ';
$lang['Login']= 'Login ';
$lang['Product'] = 'Product';
$lang['Follow us'] = 'Follow us';
$lang['Dont have an account?'] = 'Dont have an account?';
//$lang['Sign up'] = 'اتبعنا';
$lang['Forgot your password?'] = 'Forgot your password?';

$lang['Registration'] ='Registration';
$lang['first name'] ='first name';
$lang['last name'] ='last name';
$lang['phone'] ='phone ';
$lang['Email*'] ='Email*';
$lang['username'] ='username';
$lang['Photo'] ='Photo';
$lang['DetailProd']= 'DetailProd';
$lang['Product'] = 'Product';
$lang['Follow us'] = 'Follow us';
$lang['owner_post'] = 'owner_post';
$lang['Member since'] = 'Member since';
$lang['Evaluator At']			= 'Evaluator At';
$lang['learn more']= 'learn more';
$lang['Product code']			= 'Product code';
$lang['Product Name']			= 'Product Name';
$lang['Login and Contact the Seller']			= 'Login and Contact the Seller';
$lang['Price'] = 'Price';
$lang['Description'] = 'Description';
$lang['Product Information'] = 'Product Information';
$lang['Brand'] = 'Brand';
$lang['Contact the Seller'] = 'Contact the Seller';
$lang['List Evaluator'] = 'List Evaluator';
$lang['Add Evaluator'] = 'Add Evaluator';
$lang['Contact the Evaluator'] = 'Contact the Evaluator';
$lang['Product description'] = 'Product description';
$lang['Ad'] = 'Ad';
$lang['first name'] ='first name';
$lang['last name'] ='last name';
$lang['phone'] ='phone';
$lang['Email*'] ='Email*';
$lang['username'] ='username';
$lang['Photo'] ='Photo';
$lang['Confirm Password*'] ='Confirm Password*';
$lang['Password'] ='Password';
$lang['Submit'] ='Submit';
$lang['Name'] ='Name';
$lang['phoneNumber'] ='phoneNumber ';
$lang['Log out']= 'Log out ';
$lang['Discover more']='Discover more';
$lang['Today Deals']='Today Deals';
$lang['Today Needs']='Today Needs';
$lang['No Deals Today']='No Deals Today';
$lang['No Needs Today']='No Needs Today';
$lang['Sign up']= 'Sign up';
$lang['Sign In']= 'Sign In';
$lang['Contact']= 'Contact';
$lang['Follow us'] = 'Follow us';
$lang['Login']= 'Login ';
$lang['Category']= 'Category';
$lang['Trending Ads']= 'Trending Ads';
//$lang['Ad Evaluation']='Ad Evaluation';
$lang['Welcome to']= 'Welcome to';

$lang['a commercial services']= 'commercial services';
$lang['Arabic, Saudi Arabia, Riyadh ']= 'Arabic, Saudi Arabia, Riyadh';
$lang['Riadh ,9584']= 'Riadh ,9584';
$lang['Sell or Advertise anything online with Resale']='Sell or Advertise anything online with Resale ';
$lang['!']= '!';
$lang['Buy']= 'Buy'; 
$lang['Sale']= 'Sale';
$lang['view']			= 'view';
$lang['Tell us what you feel']= 'Tell us what you feel ';
$lang['Feel free to contact us']= 'Feel free to contact us ';
$lang['User name']= 'User name ';
$lang['Email']= 'Email ';
$lang['Phone']= 'Phone ';
$lang['Contact us']= 'Contact us ';
$lang['Your Message']= 'Your Message ';
$lang['Google Play']= 'Google Play';
$lang['App Store']= 'App Store';




