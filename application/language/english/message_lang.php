<?php
/*Web general*/
$lang['home']='Home';
$lang['aboutus']='About Us';
$lang['services']='Services';
$lang['quote']='Request Quote';

/*Nav*/
$lang['projects']='Portfolio';
$lang['contactus']='Contact Us';
$lang['sujet']='Subject';
$lang['slider2']= 'and website development';
$lang['pslider']='Web Design, Corporate Branding, E-Marketing Services and Search Engines, Customizing Websites for Search Engine Standards ...';

/*Constants for Intro*/

$lang['button']='Contact Us';

$lang['process']='Read more ';
$lang['pcontent1']='Request Quote';
$lang['content1']='
We are working hard to launch a bunch of premium digital products';

/*Constants for Firt section*/
$lang['content2']='
Our mission is to provide the best e-business solutions and professional services, specially designed according to the needs of companies
';
$lang['content3']='Websites are still the primary Ecommerce center in our Arab region, the source of the news and the most popular information, so we are keen to provide more attention to it through skilled programmers and renewable technical tools.
';
$lang['pcontent3']='You have special products, but you can t convince your customers how distinctive your product is? Can customers reach you but no one buys?';

/*Constants for Second section*/
$lang['taboutus']='Softify is an innovative and dynamic global digital agency focused on media with outstanding experience in web and mobile solutions, web marketing strategies and Cloud hosting solutions.
';
$lang['c1']='Line of Code';
    /*Constants sub*/
    $lang['c2']='Completed Projects';
    $lang['c4']='Professional call';
    $lang['c3']='Happy Clients';
    $lang['success']='Success';
    $lang['pcontent2']='We are an innovative and dynamic global digital agency focused on media with outstanding experience in web and mobile solutions, web marketing strategies and Cloud hosting solutions.
';
$lang['s1']='E-commerce';

/*Constants for Get Started*/
$lang['s2']='Mobile Applications';
$lang['s3']='Web Development';
$lang['s4']='SEO Optimisation';
$lang['s5']='Digital Marketing';

/*Constants for Footer*/
$lang['s6']='Web Hosting';
$lang['pdes1']='
Our belief in creating tangible value for our customers Advanced Technologies works with you step-by-step to prove your company vision] business identity and technological goals.';

$lang['ades2']='An integrated management system, offers a range of business applications that help to manage corporate projects of all sizes.';
$lang['ades3']='Create an attractive and easy-to-use design that helps you grow your business in a seamless way where the user visiting your website can achieve what he needs.';

$lang['des2']='A leading enterprise specializing in web design, e-commerce';

$lang['ades4']='
Development of smart phone applications
';

$lang['title_contact']='Our Partners';
$lang['ades6']='Web Development';
$lang['p1']='Planning';
$lang['pp1']='Establishing of communication with the client helps to identify the objectives and initial steps of site design.';
$lang['p2']='Design and development';
$lang['pp2']='It is very important to ensure that all services of the site operate efficiently from the content of the site, the speed of the download, the compatibility of the site with mobile devices';
$lang['p3']='The final product';
$lang['pp3']='Do more tests and evaluations after the completion of the site, the assurance of the efficiency of the site after completion is very important.';
$lang['testimonials']= 'Recommendations';
$lang['avis'] ='See what our customers have said about our services';
$lang['name']='Name';
$lang['email']='Email';
$lang['phone']='Phone';
$lang['message']='Write Your Message';
$lang['send']='Send';
$lang['address']='Eastern Ring Road between Exit 13 and Exit 14 Al Rabwah District Riyadh';
$lang['newsletter']='Newsletter';
$lang['atit'] = 'Advanced Technologies is a leading development and hosting company in Riyadh] Saudi Arabia specializing in web design, e-commerce, smartphone application development] and Odoo development. We offer reliable hosting and ranges at competitive prices.
';

$lang['english']='English';
$lang['arabic']='عربي';
$lang['pquote']='Website design and development and e-marketing is the fastest and most effective way to grow your business.';
$lang['pname']='Partner Name ';
$lang['function']='Function';
$lang['empnumber']='Employees Number';
$lang['send']='Send ';
?>
