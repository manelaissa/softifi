<!DOCTYPE html>
<html lang="ar">


<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>atit-sa</title>



<!-- favicon icon -->
<link rel="shortcut icon" href="<?php echo base_url() ?>assets/images/favicon.ico" />

<!-- inject css start -->

<!--== bootstrap -->
<link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

<!--== animate -->
<link href="<?php echo base_url() ?>assets/css/animate.css" rel="stylesheet" type="text/css" />

<!--== fontawesome -->
<link href="<?php echo base_url() ?>assets/css/fontawesome-all.css" rel="stylesheet" type="text/css" />

<!--== themify -->
<link href="<?php echo base_url() ?>assets/css/themify-icons.css" rel="stylesheet" type="text/css" />

<!--== audioplayer -->
<link href="<?php echo base_url() ?>assets/css/audioplayer/media-player.css" rel="stylesheet" type="text/css" />

<!--== magnific-popup -->
<link href="<?php echo base_url() ?>assets/css/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css" />

<!--== owl-carousel -->
<link href="<?php echo base_url() ?>assets/css/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css" />

<!--== base -->
<link href="<?php echo base_url() ?>assets/css/base.css" rel="stylesheet" type="text/css" />

<!--== shortcodes -->
<link href="<?php echo base_url() ?>assets/css/shortcodes.css" rel="stylesheet" type="text/css" />

<!--== default-theme -->
<link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet" type="text/css" />

<!--== responsive -->
<link href="<?php echo base_url() ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
<style>
  .openerp_enterprise_pricing_app_real_name.text-truncate {

    margin-right: 20px;
}
  </style>

<!-- inject css end -->

</head>

<body>

<!-- page wrapper start -->

<div class="page-wrapper">

<!-- preloader start -->

<div id="ht-preloader">
  <div class="loader clear-loader">
    <div class="loader-text">Loading</div>
    <div class="loader-dots"> <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
</div>

<!-- preloader end -->


<header id="site-header" class="header">
  <div id="header-wrap">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <!-- Navbar -->
          <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand logo" href="index.html">
              <img id="logo-img" class="img-center" src="<?php echo base_url() ?>assets/images/logo.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation"> <span></span>
              <span></span>
              <span></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
              <ul class="navbar-nav ml-auto mr-auto">
                <!-- Home -->
                <li class="nav-item " data-toggle="hover"> <a class="nav-link active " href="index.html" role="button"  aria-haspopup="true" >الرئيسية</a>
                
                </li>
                <li class="nav-item " data-toggle="hover"> <a class="nav-link " href="#about"   aria-haspopup="true" aria-expanded="false">من نحن </a>
               
                </li>
              
                <li class="nav-item " data-toggle="hover"> <a class="nav-link " href="#services"  aria-haspopup="true" aria-expanded="false"> خدماتنا  </a>
                 
                </li>

                 <li class="nav-item " data-toggle="hover"> <a class="nav-link " href="#quote"  aria-haspopup="true" aria-expanded="false"> أطلب السعر  </a>
                 
                </li>
                <li class="nav-item " data-toggle="hover"> <a class="nav-link " href="#blog"  aria-haspopup="true" aria-expanded="false">أعمالنا</a>
                  
                </li>
                <li class="nav-item dropdown" data-toggle="hover"> <a class="nav-link" href="#contact"   aria-haspopup="true" aria-expanded="false">اتصل بنا</a>
                 
                </li>
              </ul>
            </div>
            <div class="right-nav align-items-center d-flex justify-content-end list-inline"> 
              <div class="search">
                <div class="search-content">
                  <div class="search-button"> <i class="fas fa-search"></i>
                  </div>
                  <form id="search-form" class="search-form">
                    <input type="search" class="search-input" placeholder="Search Here...">
                  </form>
                </div>
              </div> <a href="#" class="ht-nav-toggle"><span></span></a>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>
</header>

<nav id="ht-main-nav"> <a href="#" class="ht-nav-toggle active"><span></span></a>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <img class="img-fluid side-logo mb-3" src="<?php echo base_url() ?>assets/images/logo.png" alt="">
        <p class="mb-5">شركة التقنيات المتقدمة مؤسسة رائدة في مجال التطوير والاستضافة في الرياض ، المملكة العربية السعودية متخصصة في تصميم المواقع الإلكترونية ، التجارة الإلكترونية ، تطوير تطبيقات الهواتف الذكية ، وتطوير Odoo. نحن نقدم استضافات و نطاقات موثوقة وبأسعار تنافسية.</p>
        <div class="form-info">
          <h4 class="title">اتصل بنا</h4>
          <ul class="contact-info list-unstyled mt-4">
            <li class="mb-4"><i class="flaticon-location"></i><span></span>
              <p>الطريق الدائري الشرقي بين المخرج 13 والمخرج 14 حي الربوة بالرياض</p>
            </li>
            <li class="mb-4"><i class="flaticon-call"></i><span>Phone:</span><a href="tel:+966 55 440 2288">+966 55 440 2288</a>
            </li>
            <li><i class="flaticon-email"></i><span>Email</span><a href="mailto:contact@atit-sa.com">contact@atit-sa.com</a>
            </li>
          </ul>
        </div>
        <div class="social-icons social-colored mt-5">
          <ul class="list-inline">
            <li class="mb-2 social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a>
            </li>
            <li class="mb-2 social-twitter"><a href="#"><i class="fab fa-twitter"></i></a>
            </li>
            <li class="mb-2 social-linkedin"><a href="#"><i class="fab fa-linkedin-in"></i></a>
            </li>
            <li class="mb-2 social-gplus"><a href="#"><i class="fab fa-google-plus-g"></i></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</nav>

<!--header end-->


<!--hero section start-->

<section class="fullscreen-banner banner p-0 o-hidden" data-bg-img="<?php echo base_url() ?>assets/images/pattern/01.png">
  <div class="insideText">ATIT SA</div>
  <div class="align-center">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-lg-6 col-md-12 order-lg-12">
          <div class="mouse-parallax">
            <div class="bnr-img1 animated fadeInLeft delay-4 duration-4">
              <!-- <img class="img-center rotateme" src="images/banner/05.png" alt=""> -->
            </div>
            <img class="img-center animated zoomIn delay-5 duration-4 img-fluid topBottom" src="<?php echo base_url() ?>assets/images/banner/05.png" alt="">
          </div>
        </div>
        <div class="col-lg-6 col-md-12 order-lg-1 md-mt-5">
          <h1 class="mb-4 animated bounceInRight delay-2 duration-4">نوفر أعلى جودة في تصميم  <span class="font-w-5">وتطوير مواقع الإنترنت</span> </h1>
          <p class="lead animated fadeInUp delay-3 duration-4">تصميم المواقع، العلامات التجارية للشركات، خدمات التسويق الإلكتروني ومحركات البحث، تهيئة المواقع الإلكترونية للتوافق مع معايير محركات البحث...</p>
         
        </div>        
      </div>
    </div>
   </div>
</section>

<!--hero section end-->





<!--body content start-->

<section class="grey-bg" data-bg-img="<?php echo base_url() ?>assets/images/bg/02.png" id="quote">
  <div class="container">
    <div class="row text-center">
      <div class="col-lg-8 col-md-12 ml-auto mr-auto">
        <div class="section-title">
          <h6>اطلب السعر  </h6>
          <h2 class="title">تصميم وتطوير المواقع والتسويق الإلكتروني هي الطريقة الأسرع والأكثر فعالية لتنمية أعمالك. نقوم بادارة موقعك الالكترونى والحملات الاعلانية لانتشاره مما يوفر لك المزيد من الوقت للتركيز على عملك فقط.</h2>
        </div>
      </div>
    </div>

<div class="container">
          <div class="row">
            <div class="col-lg-4 col-md-4 ">
              <div class="section-title mb-2">
              
                <h2>طلب السعر </h2>
              </div>
              <div class="contact-main">
                <form  class="row" method="POST" action="">
                  <div class="messages"></div>
                  <div class="form-group col-md-6">
                    <input id="form_name" type="text" name="name" class="form-control" placeholder="الإسم" required="required" data-error="Name is required.">
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group col-md-6">
                    <input id="form_email" type="text" name="partner" class="form-control" placeholder="إسم الشركة " required="required" data-error="Valid email is required.">
                    <div class="help-block with-errors"></div>
                  </div> 
                   <div class="form-group col-md-12">
                    <input id="form_email" type="email" name="email" class="form-control" placeholder="البريد الالكتروني " required="required" data-error="Valid email is required.">
                    <div class="help-block with-errors"></div>
                  </div>  
                  <div class="form-group col-md-12">
                    <input id="form_phone" type="tel" name="mobile" class="form-control" placeholder="الهاتف " required="required" data-error="Phone is required">
                    <div class="help-block with-errors"></div>
                  </div>

                   <div class="form-group col-md-12">
                    <input id="form_phone" type="text" name="function" class="form-control" placeholder="الوظيفة" required="required" data-error="Phone is required">
                    <div class="help-block with-errors"></div>
                  </div> 
                     <div class="form-group col-md-12">
                    <input id="form_phone" type="text" name="employees_number" class="form-control" placeholder="عدد الموظفين" required="required" data-error="Phone is required">
                    <div class="help-block with-errors"></div>
                  </div> 

                 
                 
             
              </div>
            </div>
            <div  class="col-md-8">


            <section class="openerp_enterprise_pricing position-relative" style="padding: 0 0;">
                
                <div class="row">
                  <div class="col-12 col-md-12 col-lg-12">
                    
                    <div class="openerp_enterprise_pricing_step">
                      <div class="openerp_enterprise_pricing_step_head">
                      </div>

                      <div class="openerp_enterprise_pricing_step_body mb24">
                        <div class="form-row">
                          
                  
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_crm">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/crm/static/description/icon.png" alt="CRM"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">CRM</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_crm" data-app-name="crm" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_account">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/account/static/description/icon.png" alt="Invoicing"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Invoicing</div>

           
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_account" data-app-name="account" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_sale_management">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/sale_management/static/description/icon.png" alt="Sales"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Sales</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_sale_management" data-app-name="sale_management" data-app-depends="account"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_website">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/website/static/description/icon.png" alt="Website"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Website</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_website" data-app-name="website" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_website_sale">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/website_sale/static/description/icon.png" alt="eCommerce"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">eCommerce</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_website_sale" data-app-name="website_sale" data-app-depends="account,website"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_point_of_sale">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/point_of_sale/static/description/icon.png" alt="Point of Sale"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Point of Sale</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_point_of_sale" data-app-name="point_of_sale" data-app-depends="account,stock"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_account_accountant">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/account_accountant/static/description/icon.png" alt="Accounting"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Accounting</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_account_accountant" data-app-name="account_accountant" data-app-depends="account"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_project">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/project/static/description/icon.png" alt="Project"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Project</div>

           
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_project" data-app-name="project" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_stock">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/stock/static/description/icon.png" alt="Inventory"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Inventory</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_stock" data-app-name="stock" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_mrp">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/mrp/static/description/icon.png" alt="Manufacturing"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Manufacturing</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_mrp" data-app-name="mrp" data-app-depends="stock"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_purchase">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/purchase/static/description/icon.png" alt="Purchase"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Purchase</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_purchase" data-app-name="purchase" data-app-depends="account"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_timesheet_grid">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/timesheet_grid/static/description/icon.png" alt="Timesheets"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Timesheets</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_timesheet_grid" data-app-name="timesheet_grid" data-app-depends="project"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_mass_mailing">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/mass_mailing/static/description/icon.png" alt="Email Marketing"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Email Marketing</div>

           
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_mass_mailing" data-app-name="mass_mailing" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_hr_expense">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/hr_expense/static/description/icon.png" alt="Expenses"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Expenses</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_hr_expense" data-app-name="hr_expense" data-app-depends="account"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_website_event">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/website_event/static/description/icon.png" alt="Events"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Events</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_website_event" data-app-name="website_event" data-app-depends="website"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_hr_holidays">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/hr_holidays/static/description/icon.png" alt="Time Off"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Time Off</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_hr_holidays" data-app-name="hr_holidays" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_hr_recruitment">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/hr_recruitment/static/description/icon.png" alt="Recruitment"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Recruitment</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_hr_recruitment" data-app-name="hr_recruitment" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_hr_appraisal">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/hr_appraisal/static/description/icon.png" alt="Appraisal"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Appraisal</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_hr_appraisal" data-app-name="hr_appraisal" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_sale_subscription">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/sale_subscription/static/description/icon.png" alt="Subscriptions"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Subscriptions</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_sale_subscription" data-app-name="sale_subscription" data-app-depends="account,sale_management"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_sign">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/sign/static/description/icon.png" alt="Sign"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Sign</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_sign" data-app-name="sign" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_maintenance">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/maintenance/static/description/icon.png" alt="Maintenance"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Maintenance</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_maintenance" data-app-name="maintenance" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_quality_control">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/quality_control/static/description/icon.png" alt="Quality"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Quality</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_quality_control" data-app-name="quality_control" data-app-depends="stock"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_web_studio">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/web_studio/static/description/icon.png" alt="Studio"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Studio</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_web_studio" data-app-name="web_studio" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_helpdesk">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/helpdesk/static/description/icon.png" alt="Helpdesk"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Helpdesk</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_helpdesk" data-app-name="helpdesk" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_mrp_plm">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/mrp_plm/static/description/icon.png" alt="Product Lifecycle Management (PLM)"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Product Lifecycle Management (PLM)</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_mrp_plm" data-app-name="mrp_plm" data-app-depends="stock,mrp"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_website_calendar">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/website_calendar/static/description/icon.png" alt="Appointments"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Appointments</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_website_calendar" data-app-name="website_calendar" data-app-depends="website"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_marketing_automation">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/marketing_automation/static/description/icon.png" alt="Marketing Automation"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Marketing Automation</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_marketing_automation" data-app-name="marketing_automation" data-app-depends="mass_mailing"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_documents">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/documents/static/description/icon.png" alt="Documents"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Documents</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox" name="check_list[]" id="app_documents" data-app-name="documents" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_iot">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/iot/static/description/icon.png" alt="Internet of Things"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Internet of Things</div>

           
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_iot" data-app-name="iot" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_approvals">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/approvals/static/description/icon.png" alt="Approvals"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Approvals</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_approvals" data-app-name="approvals" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_account_consolidation">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/account_consolidation/static/description/icon.png" alt="Consolidation"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Consolidation</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_account_consolidation" data-app-name="account_consolidation" data-app-depends="account,account_accountant"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_hr_referral">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/hr_referral/static/description/icon.png" alt="Employee Referral"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Employee Referral</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_hr_referral" data-app-name="hr_referral" data-app-depends="website,hr_recruitment"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_industry_fsm">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/industry_fsm/static/description/icon.png" alt="Field Service"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Field Service</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_industry_fsm" data-app-name="industry_fsm" data-app-depends="account,sale_management,project,timesheet_grid"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_planning">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/planning/static/description/icon.png" alt="Planning"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Planning</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_planning" data-app-name="planning" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_sale_renting">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/sale_renting/static/description/icon.png" alt="Rental"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Rental</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_sale_renting" data-app-name="sale_renting" data-app-depends="account"/>

            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_social">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/social/static/description/icon.png" alt="Social Marketing"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">Social Marketing</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_social" data-app-name="social" data-app-depends=""/>

            
            
          </div>
        </label>
      </div>
    
                          
                            
      <div class="col-12 col-sm-6 col-lg-4">
        <label class="card d-flex flex-row p-2 mb-2 align-items-stretch openerp_enterprise_pricing_app " for="app_website_slides">
          <div class="openerp_enterprise_pricing_app_icon pr-2 flex-shrink-0 flex-grow-0" style="max-width: 50px">
            <img class="img-fluid" src="https://download.odoocdn.com/pricing/icons/13.0/website_slides/static/description/icon.png" alt="eLearning"/>
          </div>

          <div class="d-flex flex-column flex-grow-1 flex-shrink-0 w-50 justify-content-between openerp_enterprise_pricing_app_name">
            <div class="openerp_enterprise_pricing_app_real_name text-truncate">eLearning</div>

            
          </div>

          <div class="d-flex flex-column justify-content-between align-items-center">
            <input type="checkbox"  name="check_list[]" id="app_website_slides" data-app-name="website_slides" data-app-depends="website"/>

            
          </div>
        </label>
      </div>
    
                          
                        </div>
                      </div>

                      <div class="openerp_enterprise_pricing_step_head">
                      </div>
                      <div class="openerp_enterprise_pricing_step_body mb16">
                        <div class="row">
                          
                          
                            
     
    
                          
                            
     
    
                          
                            
     
                          
                            
     
    
                          
                            
      
                          
    
                          
                        </div>
                      </div>
                    </div>

                   
 <div class="col-md-12">
 
                  <button type="Submit" class="btn btn-theme btn-radius"><span>إرسال </span>
                  </button>
                  </div>
                     </form>



                   
                  </div>





                  <?php
if ($_SERVER['REQUEST_METHOD']=='POST') {
 
  $name = htmlentities($_POST['name']);
        
  $partner     = htmlentities($_POST['partner']);
  $mobile   = htmlentities($_POST['mobile']);

  $email = htmlentities($_POST['email']);
    $function = htmlentities($_POST['function']);

  $employees_number = htmlentities($_POST['employees_number']);
  


$check_list = implode("|",$_POST['check_list']);



 
  $destinataire = 'ri'; 
  $sujet = 'You have received a new quote online'; 
  $contenu = '<html><head><title>Title of Quote</title></head><body>';
  $contenu .= '<p>Good Morning Dear ATIT ADVANCED COMPANY CO.,You have a new message from a client.</p>';
  $contenu .= '<p><strong>Contact Name</strong>: '.$name.'</p>';
  $contenu .= '<p><strong>Partner</strong>: '.$partner.'</p>';

  $contenu .= '<p><strong>Mobile</strong>: '.$mobile.'</p>';
    $contenu .= '<p><strong>Email</strong>: '.$email.'</p>';

    $contenu .= '<p><strong>Function</strong>: '.$function.'</p>';
        $contenu .= '<p><strong>Selected Modules</strong>: '.$check_list.'</p>';
            


       
       




  $contenu .= '</body></html>'; // Contenu du message de l'email (en XHTML)
 
 
  $headers = 'MIME-Version: 1.0'."\r\n";
  $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";
 
  
  mail($destinataire, $sujet, $contenu, $headers); 
    echo '<h2>لقد تم ارسال رسالتك بنجاح . سنتواصل معك بأقرب وقت ممكن.!</h2>'; 

  
}
  ?>


                  

     
          </div>
        </div>
      </div>
    </div>
  </div>
</section>







            </div>
            
          </div>
  </div>



<footer class="footer white-bg pos-r o-hidden bg-contain" data-bg-img="<?php echo base_url() ?>assets/images/pattern/01.png">
  <div class="round-p-animation"></div>
  <div class="primary-footer">
    <div class="container-fluid p-0">
      <div class="row">
        <div class="col-lg-4">
          <div class="ht-theme-info bg-contain bg-pos-r h-100 dark-bg text-white" >
            
            
            <div class="social-icons social-border circle social-hover mt-5">
              <ul class="list-inline">
                <li class="social-facebook"><a href="#"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li class="social-twitter"><a href="#"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="social-gplus"><a href="#"><i class="fab fa-google-plus-g"></i></a>
                </li>
                <li class="social-linkedin"><a href="#"><i class="fab fa-linkedin-in"></i></a>
                </li>
                <li class="social-skype"><a href="#"><i class="fab fa-skype"></i></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-8  md-px-5">
          <div class="row">
            <div class="col-lg-6 col-md-6 footer-list">
              
              <div class="row">
                <div class="col-sm-5">
                  
                </div>
                <div class="col-sm-7">
                  
                </div>
              </div>
            </div>
            
          </div>
          <div class="row mt-5">
            <div class="col-lg-10 col-md-12 ml-auto">
              <div class="align-items-center white-bg box-shadow px-3 py-3 radius d-md-flex justify-content-between">
                <h4 class="mb-0">إشترك الآن </h4>
                <div class="subscribe-form sm-mt-2">
                  <form id="mc-form" class="group">
                    <input type="email" value="" name="EMAIL" class="email" id="mc-email" placeholder="البريد الإلكتروني" required="">
                    <input class="btn btn-theme" type="submit" name="subscribe" value="إشترك الآن ">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="secondary-footer">
    <div class="container">
      <div class="copyright">
        <div class="row align-items-center">
          <div class="col-md-6"> <span>ATIT-SA 2019 | كل الحقوق محفوظة </span>
          </div>
          <div class="col-md-6 text-md-left sm-mt-2"> <span>atit-sa <a href="#"></a></span>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<!--footer end-->


</div>

<!-- page wrapper end -->


<!--back-to-top start-->

<div class="scroll-top"><a class="smoothscroll" href="#top"><i class="flaticon-upload"></i></a></div>

<!--back-to-top end-->

 
<!-- inject js start -->

<!--== jquery -->
<script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>

<!--== popper -->
<script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>

<!--== bootstrap -->
<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>

<!--== appear -->
<script src="<?php echo base_url() ?>assets/js/jquery.appear.js"></script> 

<!--== modernizr -->
<script src="<?php echo base_url() ?>assets/js/modernizr.js"></script> 

<!--== audioplayer -->
<script src="<?php echo base_url() ?>assets/js/audioplayer/media-player.js"></script>

<!--== magnific-popup -->
<script src="<?php echo base_url() ?>assets/js/magnific-popup/jquery.magnific-popup.min.js"></script> 

<!--== owl-carousel -->
<script src="<?php echo base_url() ?>assets/js/owl-carousel/owl.carousel.min.js"></script> 

<!--== counter -->
<script src="<?php echo base_url() ?>assets/js/counter/counter.js"></script> 

<!--== countdown -->
<script src="<?php echo base_url() ?>assets/js/countdown/jquery.countdown.min.js"></script> 

<!--== isotope -->
<script src="<?php echo base_url() ?>assets/js/isotope/isotope.pkgd.min.js"></script> 

<!--== mouse-parallax -->
<script src="<?php echo base_url() ?>assets/js/mouse-parallax/tweenmax.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/mouse-parallax/jquery-parallax.js"></script> 

<!--== contact-form -->
<script src="<?php echo base_url() ?>assets/js/contact-form/contact-form.js"></script>

<!--== validate -->
<script src="<?php echo base_url() ?>assets/js/contact-form/jquery.validate.min.js"></script>

<!--== map api -->
<script src="https://maps.googleapis.com/maps/api/js"></script>

<!--== map -->
<script src="<?php echo base_url() ?>assets/js/map.js"></script>

<!--== wow -->
<script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>

<!--== theme-script -->
<script src="<?php echo base_url() ?>assets/js/theme-script.js"></script>

<!-- inject js end -->

</body>




</html>





