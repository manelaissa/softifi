<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class LanguageSwitcher extends CI_Controller
{
    public function __construct() {
        parent::__construct();     
    }
    //function 'switchLang' to switch site language (english or arabic) 
    function switchLang($language = "") {
        
        $language = ($language != "") ? $language : "english";
        //set the session userdata language to $language(send by client favorite lang) 
        $this->session->set_userdata('site_lang', $language);
        
        redirect($_SERVER['HTTP_REFERER']);
        
    }
}