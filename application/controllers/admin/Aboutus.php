<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Aboutus extends CIF_Controller {

    public $layout = 'full';
    public $module = 'aboutus';
    public $model = 'Aboutus_model';

    public function __construct() {
        parent::__construct();
        $this->load->model($this->model);
        $this->_primary_key = $this->{$this->model}->_primary_keys[0];
        $this->permission();
    }

    public function manage($id = FALSE) {
        $data = array();

        if ($id) {
            $this->{$this->model}->{$this->_primary_key} = $id;
            $data['item'] = $this->{$this->model}->get();
            if (!$data['item'])
                show_404();
        } else {
            $data['item'] = new Std();
        }
        $this->load->library("form_validation");
        $this->form_validation->set_rules('about', 'Aboutus_arabic Language', 'trim|required');
        $this->form_validation->set_rules('benfits', 'Why choose us', 'trim|required');
                $this->form_validation->set_rules('aboutus_fr', 'Aboutus_french Language', 'trim|required');
        $this->form_validation->set_rules( 'aboutus_eng','Aboutus_English Language', 'trim|required');
                $this->form_validation->set_rules("image", 'Image', "trim|callback_image[$id]");


       

        if ($this->form_validation->run() == FALSE)
            $this->load->view($this->module . '/manage', $data);

        else {
            $this->{$this->model}->about = $this->input->post('about');
            $this->{$this->model}->benfits = $this->input->post('benfits');
            $this->{$this->model}->aboutus_fr = $this->input->post('aboutus_fr');
            $this->{$this->model}->aboutus_eng = $this->input->post('aboutus_eng');



            $this->{$this->model}->save();
            redirect('admin/aboutus/manage/1');
        }
    }
     public function image($var, $id) {
        $config['upload_path'] = './cdn/about/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('image')) {
            $data = $this->upload->data();
            if ($data['file_name'])
                $this->{$this->model}->image = base_url() . '/cdn/about/' . $data['file_name'];
        }
        return true;
    }

   

}
