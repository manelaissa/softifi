
<?php
//index.php
ini_set('default_charset', 'UTF-8');

$error = '';
$name = '';
$email = '';
$mobile = '';
$message = '';
$subject='';

function clean_text($string)
{
  $string = trim($string);
  $string = stripslashes($string);
  $string = htmlspecialchars($string);
  return $string;
}

if(isset($_POST["submit"]))
{
  if(empty($_POST["name"]))
  {
    $error .= '<p><label class="text-danger">Please Enter your Name</label></p>';
  }
  else
  {
    $name = clean_text($_POST["name"]);
    
  }
  if(empty($_POST["subject"]))
  {
    $error .= '<p><label class="text-danger">Please Enter your Name</label></p>';
  }
  else
  {
    $subject = clean_text($_POST["subject"]);
    
  }
  if(empty($_POST["email"]))
  {
    $error .= '<p><label class="text-danger">Please Enter your Email</label></p>';
  }
  else
  {
    $email = clean_text($_POST["email"]);
    if(!filter_var($email, FILTER_VALIDATE_EMAIL))
    {
      $error .= '<p><label class="text-danger">Invalid email format</label></p>';
    }
  }
  if(empty($_POST["mobile"]))
  {
    $error .= '<p><label class="text-danger">Subject is required</label></p>';
  }
  else
  {
    $mobile = clean_text($_POST["mobile"]);
  }
  
  if(empty($_POST["message"]))
  {
    $error .= '<p><label class="text-danger">Message is required</label></p>';
  }
  else
  {
    $message = clean_text($_POST["message"]);
  }
  if($error == '')
  {
    require 'class/class.phpmailer.php';
    $mail = new PHPMailer;
    $mail->IsSMTP();                //Sets Mailer to send message using SMTP
    $mail->Host = 'smtp.mailtrap.io';   //Sets the SMTP hosts of your Email hosting, this for Godaddy
    $mail->Port = '587';                //Sets the default SMTP server port
    $mail->SMTPAuth = true;             //Sets SMTP authentication. Utilizes the Username and Password variables
    $mail->Username = '1b4d1a1518ac17';         //Sets SMTP username
    $mail->Password = '4a898f641ce64a';         //Sets SMTP password
    $mail->SMTPSecure = '';   
     $mail->CharSet = 'UTF-8';
    $mail->Encoding = 'base64';          //Sets connection prefix. Options are "", "ssl" or "tls"
    $mail->From = $_POST["email"];          //Sets the From email address for the message
    $mail->FromName = $_POST["name"];       //Sets the From name of the message
    $mail->AddAddress('riadhfraj23@gmail.com', 'Name');   //Adds a "To" address
    $mail->AddCC($_POST["email"], $_POST["name"]);  //Adds a "Cc" address
    $mail->WordWrap = 50;             //Sets word wrapping on the body of the message to a given number of characters
    $mail->IsHTML(true);              //Sets message type to HTML       
    $mail->Subject = 'لقد تلقيت رسالة جديدة ';       //Sets the Subject of the message
    $mail->Body =(
          'Name : &nbsp;'  . $name.' <br>Mobile : &nbsp;' . $mobile.'
         <br> Email Address : &nbsp;' . $email.'
         <br> Subject : &nbsp;' .$subject.'
          <br> Message : &nbsp;' .$message.''
         );




         //An HTML or plain text message body
    if($mail->Send())               //Send an Email. Return true on success or false on error
    {
     $error = '<label class="text-success"> <h4>لقد تم ارسال رسالتك بنجاح . سنتواصل معك بأقرب وقت ممكن.! </h4>
</label>';   
    }
    else
    {
      $error = '<label class="text-danger"> <h4> هنالك خطأ الرجاء إعادة المحاولة 
</h4> </label>';
    }
    $name = '';
    $email = '';
    $mobile = '';
    $message = '';
    $subject='';
  }
}

?>





<!doctype html>
<html   lang="en">


<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Softifi est une agence de création de site internet qui est spécialisée dans la création de site web.Notre agence web en Tunisie met à votre disposition une équipe d’experts et de développeur web au service de votre performance.
</title>
    <meta name="description" content="<?php echo config('meta_description') ?>">
        <meta name="Keywords" content="<?php echo config('meta_keywords') ?>">
        <link rel="shortcut icon" href="<?php echo config('favicon') ?>" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() ?>assets/img/softifi.jpg">
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/meanmenu.min.css">
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/normalize.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/animate.min.css">
    <!-- Font-flat CSS-->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/fonts/flaticon.css">
    <!-- Owl Caousel CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/OwlCarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/OwlCarousel/owl.theme.default.min.css">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/slider/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/slider/css/preview.css" type="text/css" media="screen" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
    <script src="<?php echo base_url() ?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<style type="text/css">
                        



</style>
</head>

<body>
   
    <div class="wraper">
        <header id="home">
            <div class="header-two">
                <div class="header-top-area" id="sticker">
                    <div class="container">
                        <div class="row">
                                                                <?php if($this->session->userdata('site_lang')=='arabic'){ ?>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="float: right;">
                                <div class="logo-area">
                                    <a href="<?php echo base_url() ?>"><img class="img-responsive" style="float: right;" src="<?php echo base_url() ?>assets/img/softifilogo.jpg" alt="logo"></a>
                                </div>
                            </div>
                                                                                  <?php }elseif($this->session->userdata('site_lang')=='english'){ ?>
                                      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="float: left;">
                                <div class="logo-area">                                              
                                    <a href="<?php echo base_url() ?>"><img class="img-responsive" style="float: left;" src="<?php echo base_url() ?>assets/img/softifilogo.jpg" alt="logo"></a>
                                </div>
                            </div>
                          <?php }elseif($this->session->userdata('site_lang')=='french'){ ?>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="float: left;">
                                <div class="logo-area">                                              
                                    <a href="<?php echo base_url() ?>"><img class="img-responsive" style="float: left;" src="<?php echo base_url() ?>assets/img/softifilogo.jpg" alt="logo"></a>
                                </div>
                            </div>

                                              <?php } else{ ?>
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="float: left;">
                                <div class="logo-area"> 
                                    <a href="<?php echo base_url() ?>"><img class="img-responsive" style="float: left;" src="<?php echo base_url() ?>assets/img/softifilogo.jpg" alt="logo"></a>
</div>
</div>
                            <?php } ?>

                            
                            <?php if($this->session->userdata('site_lang')=='arabic'){ ?>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <div class="main-menu">
                                    <nav>
                                        <ul id="navOnePage">

                                           <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/english"><?php echo $this->lang->line('english'); ?></a>
                                            <ul>
  <li ><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/french"><?php echo $this->lang->line('french'); ?></a></li>

 </ul>
</li>
                                           </li>
                                             <li><a href="#contact"><?php echo $this->lang->line('contactus'); ?></a></li>
                                           
                                            <li><a href="#projects"><?php echo $this->lang->line('projects'); ?></a></li>
                                            <li><a href="#service"><?php echo $this->lang->line('services'); ?></a></li>

                                             <li><a href="#about"><?php echo $this->lang->line('aboutus'); ?></a></li>
                                             <li>
                                                <a href="index.php"><?php echo $this->lang->line('home'); ?> </a>
                                               
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                                              <?php }elseif($this->session->userdata('site_lang')=='english'){ ?>
                                                <style type="text/css">
                                                  html,
body {
  height: 100%;
  font-family: 'Niramit', sans-serif;
  font-size: 15px;
  line-height: 24px;
  font-weight: 400;
  vertical-align: baseline;

  color: #707070;
}
                                                </style>
 <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <div class="main-menu">
                                    <nav>
                                        <ul id="navOnePage">
<li>
                                                <a href="index.php"><?php echo $this->lang->line('home'); ?> </a>
                                               
                                            </li>
                                            <li><a href="#about"><?php echo $this->lang->line('aboutus'); ?></a></li>
                                             <li><a href="#service"><?php echo $this->lang->line('services'); ?></a></li>
                                              <li><a href="#projects"><?php echo $this->lang->line('projects'); ?> </a></li>
                                             <li><a href="#contact"><?php echo $this->lang->line('contactus'); ?></a></li>
                                            <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/french"><?php echo $this->lang->line('french'); ?>
</a>
<ul>
                                            
  <li ><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/arabic"><?php echo $this->lang->line('arabic'); ?></a></li>

 </ul>
</li>




                                            
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                          <?php }elseif($this->session->userdata('site_lang')=='french'){ ?>
                             <style type="text/css">
                                                  html,
body {
  height: 100%;
  font-family: 'Niramit', sans-serif;
  font-size: 15px;
  line-height: 24px;
  font-weight: 400;
  vertical-align: baseline;

  color: #707070;
}
                                                </style>
                             <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <div class="main-menu">
                                    <nav>
                                        <ul id="navOnePage">
<li>
                                                <a href="index.php"><?php echo $this->lang->line('home'); ?> </a>
                                               
                                            </li>
                                            <li><a href="#about"><?php echo $this->lang->line('aboutus'); ?></a></li>
                                             <li><a href="#service"><?php echo $this->lang->line('services'); ?></a></li>
                                              <li><a href="#projects"><?php echo $this->lang->line('projects'); ?></a></li>
                                             <li><a href="#contact"><?php echo $this->lang->line('contactus'); ?></a></li>
                                              <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/arabic"><?php echo $this->lang->line('arabic'); ?>
</a>
<ul>
                                            
  <li ><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/english"><?php echo $this->lang->line('english'); ?></a></li>

 </ul>
</li>







                                            
                                        </ul>
                                    </nav>
                                </div>
                            </div>

                                              <?php } else{ ?>
                                                 <style type="text/css">
                                                  html,
body {
  height: 100%;
  font-family: 'Niramit', sans-serif;
  font-size: 15px;
  line-height: 24px;
  font-weight: 400;
  vertical-align: baseline;

  color: #707070;
}
                                                </style>
                          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <div class="main-menu">
                                    <nav>
                                        <ul id="navOnePage">
<li>
                                                <a href="index.php"><?php echo $this->lang->line('home'); ?> </a>
                                               
                                            </li>
                                            <li><a href="#about"><?php echo $this->lang->line('aboutus'); ?></a></li>
                                             <li><a href="#service"><?php echo $this->lang->line('services'); ?></a></li>
                                              <li><a href="#projects"><?php echo $this->lang->line('projects'); ?></a></li>
                                             <li><a href="#contact"><?php echo $this->lang->line('contactus'); ?></a></li>
                                            <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/arabic"><?php echo $this->lang->line('arabic'); ?>
</a>
<ul>
                                            
  <li ><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/english"><?php echo $this->lang->line('english'); ?></a></li>

 </ul>
</li>

                                            
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
                                                                            <?php if($this->session->userdata('site_lang')=='arabic'){ ?>

            <!-- mobile-menu-area start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                        <li>
                                                <a href="index.php">الرئيسية </a>
                                               
                                            </li>
                                            <li><a href="#about">من نحن  </a></li>

                                            <li><a href="#service">خدماتنا</a></li>
                                            <li><a href="#projects">معرض أعمالنا </a></li>
                                            <li><a href="#contact">اتصل بنا</a></li>
                                             <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/english"><?php echo $this->lang->line('english'); ?></a></li>
                                        </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                                                                                              <?php }elseif($this->session->userdata('site_lang')=='english'){ ?>
                                                                                                         <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                     <li>
                                                <a href="index.php"><?php echo $this->lang->line('home'); ?> </a>
                                               
                                            </li>
                                            <li><a href="#about"><?php echo $this->lang->line('aboutus'); ?></a></li>
                                             <li><a href="#service"><?php echo $this->lang->line('services'); ?></a></li>
                                              <li><a href="#projects">Our Projects </a></li>
                                             <li><a href="#contact"><?php echo $this->lang->line('contactus'); ?></a></li>
                                            <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/arabic"><?php echo $this->lang->line('arabic'); ?>
</a></li>
                                        </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php }elseif($this->session->userdata('site_lang')=='french'){ ?>
                                                                                                                       <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                     <li>
                                                <a href="index.php"><?php echo $this->lang->line('home'); ?> </a>
                                               
                                            </li>
                                            <li><a href="#about"><?php echo $this->lang->line('aboutus'); ?></a></li>
                                             <li><a href="#service"><?php echo $this->lang->line('services'); ?></a></li>
                                              <li><a href="#projects"><?php echo $this->lang->line('projects'); ?> </a></li>
                                             <li><a href="#contact"><?php echo $this->lang->line('contactus'); ?></a></li>
                                            <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/arabic"><?php echo $this->lang->line('arabic'); ?>
</a></li>
                                        </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                                                          <?php } else{ ?>
                                                                  <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                     <li>
                                                <a href="index.php"><?php echo $this->lang->line('home'); ?> </a>
                                               
                                            </li>
                                            <li><a href="#about"><?php echo $this->lang->line('aboutus'); ?></a></li>
                                             <li><a href="#service"><?php echo $this->lang->line('services'); ?></a></li>
                                              <li><a href="#projects"><?php echo $this->lang->line('projects'); ?></a></li>
                                             <li><a href="#contact"><?php echo $this->lang->line('contactus'); ?></a></li>
                                            <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/arabic"><?php echo $this->lang->line('arabic'); ?>
</a></li>
                                        </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>


            <!-- mobile-menu-area end -->
        </header>
        <!-- Header Area End Here -->

        <!-- Slider 1 Area Start Here -->
         <?php if($this->session->userdata('site_lang')=='arabic'){ ?>

        <?php foreach ($sliders as $item): ?>  
            
                            
        <div class="slider-default slider-overlay">
            <div class="bend niceties preview-1">
                <div id="ensign-nivoslider-3" class="slides">
                    <img src="<?php echo $item->image ?>" alt="slider" title="#slider-direction-1" />
                    
                </div>
                <div id="slider-direction-1" class="t-cn slider-direction">
                    <div class="slider-content s-tb slide-1 container">
                        <div class="title-container s-tb-c">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb30-xs">
                        <div class="content-part">
                            <p class="slider-big-text" ><?php echo $item->title ?></p>
                            <p><?php echo $item->description ?>

</p>
<div class="slider-btn-area" align="center">
                                <a href="#contact" class="ghost-btn"><?php echo $this->lang->line('button'); ?></a>  
                            </div>

                        </div>
                    </div>
                  
                            
                            
                        </div>
                    </div>
                </div>
                                <?php endforeach ?>

              
            </div>
        </div>
        <?php }elseif($this->session->userdata('site_lang')=='english'){ ?>

        <?php foreach ($sliders as $item): ?>  
            
                            
        <div class="slider-default slider-overlay">
            <div class="bend niceties preview-1">
                <div id="ensign-nivoslider-3" class="slides">
                    <img src="<?php echo $item->image ?>" alt="slider" title="#slider-direction-1" />
                    
                </div>
                <div id="slider-direction-1" class="t-cn slider-direction">
                    <div class="slider-content s-tb slide-1 container">
                        <div class="title-container s-tb-c">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb30-xs">
                        <div class="content-part">
                            <p class="slider-big-text" ><?php echo $item->title_en ?></p>
                            <p><?php echo $item->description_en ?>

</p>
<div class="slider-btn-area" align="center">
                                <a href="#contact" class="ghost-btn"><?php echo $this->lang->line('button'); ?></a>  
                            </div>

                        </div>
                    </div>
                  
                            
                            
                        </div>
                    </div>
                </div>
                                <?php endforeach ?>

              
            </div>
        </div>
<?php }elseif($this->session->userdata('site_lang')=='french'){ ?>

        <?php foreach ($sliders as $item): ?>  
            
                            
        <div class="slider-default slider-overlay">
            <div class="bend niceties preview-1">
                <div id="ensign-nivoslider-3" class="slides">
                    <img src="<?php echo $item->image ?>" alt="slider" title="#slider-direction-1" />
                    
                </div>
                <div id="slider-direction-1" class="t-cn slider-direction">
                    <div class="slider-content s-tb slide-1 container">
                        <div class="title-container s-tb-c">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb30-xs">
                        <div class="content-part">
                            <p class="slider-big-text" ><?php echo $item->title_fr ?></p>
                            <p><?php echo $item->description_fr ?>

</p>
<div class="slider-btn-area" align="center">
                                <a href="#contact" class="ghost-btn"><?php echo $this->lang->line('button'); ?></a>  
                            </div>

                        </div>
                    </div>
                  
                            
                            
                        </div>
                    </div>
                </div>
                                <?php endforeach ?>

              
            </div>
        </div>
         <?php } else{ ?>

        <?php foreach ($sliders as $item): ?>  
            
                            
        <div class="slider-default slider-overlay">
            <div class="bend niceties preview-1">
                <div id="ensign-nivoslider-3" class="slides">
                    <img src="<?php echo $item->image ?>" alt="slider" title="#slider-direction-1" />
                    
                </div>
                <div id="slider-direction-1" class="t-cn slider-direction">
                    <div class="slider-content s-tb slide-1 container">
                        <div class="title-container s-tb-c">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb30-xs">
                        <div class="content-part">
                            <p class="slider-big-text" ><?php echo $item->title_fr ?></p>
                            <p><?php echo $item->description_fr ?>

</p>
<div class="slider-btn-area" align="center">
                                <a href="#contact" class="ghost-btn"><?php echo $this->lang->line('button'); ?></a>  
                            </div>

                        </div>
                    </div>
                  
                            
                            
                        </div>
                    </div>
                </div>
                                <?php endforeach ?>

              
            </div>
        </div>
                <?php } ?>


        <!-- Slider 1 Area End Here -->

        <?php if($this->session->userdata('site_lang')=='arabic'){ ?>

 <div id="service" class="home-five-service padding-top-bottom">
            <div class="container">
         <div class="row">
        <?php foreach ($services as $item): ?>    

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="single-service mb30-xs">
                            <div class="service-img" >
                  
   

 <i class="<?php echo $item->icone?>" style="font-size: 44px;" aria-hidden="true"> </i>

 

                                
                            </div>
                            <div class="media-body content">
                                <h3>                                <?php echo $item->title ?>
</h3>
                
                                <p> <?php foreach (explode("\n", $item->description) as $i): ?>
                        <p> <?php echo $i ?></p>
                        <a href="<?php echo site_url('service/index/' . $item->service_id) ?>" class="ghost-btn"><?php echo $this->lang->line('process'); ?> </a>  
                    <?php endforeach; ?></p>
                            </div>
                        </div>
                    </div>
        
        <?php endforeach ?>
    </div><!--/ Team row end -->
</div>
</div>
<?php }elseif($this->session->userdata('site_lang')=='english'){ ?>

   <div id="service" class="home-five-service padding-top-bottom">
            <div class="container">
         <div class="row">
        <?php foreach ($services as $item): ?>    

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="single-service mb30-xs">
                            <div class="service-img">
                  
   

 <i class="<?php echo $item->icone?>"  aria-hidden="true"> </i>

 

                                
                            </div>
                            <div class="media-body content">
                                <h3>                                <?php echo $item->title_en ?>
</h3>
                
                                <p> <?php foreach (explode("\n", $item->description_en) as $i): ?>
                        <p> <?php echo $i ?></p>
                        <a href="<?php echo site_url('service/index/' . $item->service_id) ?>" class="ghost-btn"><?php echo $this->lang->line('process'); ?></a>  
                    <?php endforeach; ?></p>
                            </div>
                        </div>
                    </div>
        
        <?php endforeach ?>
    </div><!--/ Team row end -->
</div>
</div>
<?php }elseif($this->session->userdata('site_lang')=='french'){ ?>

   <div id="service" class="home-five-service padding-top-bottom">
            <div class="container">
         <div class="row">
        <?php foreach ($services as $item): ?>    

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="single-service mb30-xs">
                            <div class="service-img">
                  
   

 <i class="<?php echo $item->icone?>"  aria-hidden="true"> </i>

 

                                
                            </div>
                            <div class="media-body content">
                                <h3>                                <?php echo $item->title_fr ?>
</h3>
                
                                <p> <?php foreach (explode("\n", $item->description_fr) as $i): ?>
                        <p> <?php echo $i ?></p>
                        <a href="<?php echo site_url('service/index/' . $item->service_id) ?>" class="ghost-btn"><?php echo $this->lang->line('process'); ?></a>  
                    <?php endforeach; ?></p>
                            </div>
                        </div>
                    </div>
        
        <?php endforeach ?>
    </div><!--/ Team row end -->
</div>
</div>
<?php } else{ ?>
     <div id="service" class="home-five-service padding-top-bottom">
            <div class="container">
         <div class="row">
        <?php foreach ($services as $item): ?>    

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="single-service mb30-xs">
                            <div class="service-img">
                  
   

 <i class="<?php echo $item->icone?>"  aria-hidden="true"> </i>

 

                                
                            </div>
                            <div class="media-body content">
                                <h3>                                <?php echo $item->title_fr ?>
</h3>
                
                                <p> <?php foreach (explode("\n", $item->description_fr) as $i): ?>
                        <p> <?php echo $i ?></p>
                        <a href="<?php echo site_url('service/index/' . $item->service_id) ?>" class="ghost-btn"><?php echo $this->lang->line('process'); ?></a>  
                    <?php endforeach; ?></p>
                            </div>
                        </div>
                    </div>
        
        <?php endforeach ?>
    </div><!--/ Team row end -->
</div>
</div>
 <?php } ?>

        <!-- Home Two service End Here -->
                <?php if($this->session->userdata('site_lang')=='arabic'){ ?>

<?php foreach ($aboutus as $item): ?> 
          <div class="about_us">
        <h4 style="color: #555555; text-align: center;"><?php echo $item->about ?></h4>

<img src="<?php echo $item->image ?>" style=" width: 40%;display: block;
    margin: 0 auto;" class="img-responsive" alt="what seo">   

                              

                
                               
                           
        
        <?php endforeach ?>
        <?php }elseif($this->session->userdata('site_lang')=='english'){ ?>
<?php foreach ($aboutus as $item): ?> 
          <div class="about_us">
        <h4 style="color: #555555; text-align: center;"><?php echo $item->aboutus_eng ?></h4>

<img src="<?php echo $item->image ?>" style=" width: 40%;display: block;
    margin: 0 auto;" class="img-responsive" alt="what seo">   

                              

                
                               
                           
        
        <?php endforeach ?>
                <?php }elseif($this->session->userdata('site_lang')=='french'){ ?>


     <?php foreach ($aboutus as $item): ?> 
          <div class="about_us">
        <h4 style="color: #555555; text-align: center;"><?php echo $item->aboutus_fr ?></h4>

<img src="<?php echo $item->image ?>" style=" width: 40%;display: block;
    margin: 0 auto;" class="img-responsive" alt="what seo">   

                              

                
                               
                           
        
        <?php endforeach ?>
        <?php } else{ ?>

 <?php foreach ($aboutus as $item): ?> 
          <div class="about_us">
        <h4 style="color: #555555; text-align: center;"><?php echo $item->aboutus_fr ?></h4>

<img src="<?php echo $item->image ?>" style=" width: 40%;display: block;
    margin: 0 auto;" class="img-responsive" alt="what seo">   

                              

                
                               
                           
        
        <?php endforeach ?>
      <?php } ?>







 <section id="counter-stats" >
    <div class="container">
        <div class="row">

            <div class="col-lg-3 stats">
                <h5 style="color: white;"><?php echo $this->lang->line('content1'); ?>
</h5>
            </div>

            <div class="col-lg-3 stats">
                
            </div>

            <div class="col-lg-3 stats">
                <div class="slider-btn-area">
                                <a href="#" class="ghost-btn" style="background-color: #ffffff"> <?php echo $this->lang->line('pcontent1'); ?></a>
                            </div>
                        </div>
            </div>

            <div class="col-lg-3 stats">
               
            </div>


        </div>
        <!-- end row -->
    </div>
    <!-- end container -->

</section>


  




<div class="container bootstrap snippet" id="projects">
    
    <section id="projects" class="gray-bg padding-top-bottom">    
        <div class="title-section">
                    <h2 style="color: #ac0101;"><?php echo $this->lang->line('projects'); ?></h2>
                    <h4 style="color: #555555;"><?php echo $this->lang->line('content2'); ?>
</h4> 
<br>
        <div class="container bootstrap snippet">
            <div class="categories">
                <ul>
                    <li class="active">
                        <a  data-filter="*"><?php echo $this->lang->line('s1'); ?></a>
                    </li>
                    <li>
                        <a  data-filter=".<?php echo $this->lang->line('s2'); ?>"><?php echo $this->lang->line('s2'); ?></a>
                    </li>
                    <li>
                        <a  data-filter=".<?php echo $this->lang->line('s3'); ?>"><?php echo $this->lang->line('s3'); ?></a>
                    </li>
                    <li>
                        <a  data-filter=".<?php echo $this->lang->line('s4'); ?>"><?php echo $this->lang->line('s4'); ?></a>
                    </li>
                     <li>
                        <a  data-filter=".<?php echo $this->lang->line('s5'); ?>"><?php echo $this->lang->line('s5'); ?></a>
                    </li>
                </ul>
            </div>
            
            <!-- ======= Portfolio items ===-->
            <div class="projects-container scrollimation in">
                <div class="row">
                     <?php foreach ($items as $item): ?>

                    <article class="col-md-4 col-sm-6 portfolio-item">
                        <div class="portfolio-thumb in">
                            <a href="#" class="main-link">
                                <img class="img-responsive img-center" src="<?php echo $item->image ?>" alt="">
                                <h2 class="project-title"><?php echo $item->title ?></h2>
                                <span class="overlay-mask"></span>
                            </a>
                           
                        </div>
                    </article>
                                            <?php endforeach ?>



 </div>
            </div>
        </div>  
    </section>
</div>  









      




      

         <!-- Home Two service End Here -->
        <!-- About SEO start Here -->
        <div id="about" class="about-seo-three padding-top-bottom position-relative">
            <div class="container">
                <div class="row">
                        <div class="content-part">
                             <img src="<?php echo base_url() ?>assets/img/softifiblanc.png" style="margin-right: 300px;" alt="what seo" >

                            <h4 style="color: #555555; text-align: center;"><?php echo $this->lang->line('taboutus'); ?></h4>
                            <p></p>
                        </div>
                    </div>
                    
            </div>
        </div>
        <!-- About SEO End Here -->
      






       
        <!-- Seo premium feature Start Here -->
        <div class="premium-feature-three">
            <div class="container">
  <div class="row" >


         <div class="col-lg-12 col-md-12 col-sm-4 col-xs-12">
              <div class="premium-button">
                  <img src="<?php echo base_url() ?>assets/img/mobile-softifi.png" class="imgmobile">
                
              </div>
                </div>
 </div>
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="premium-content">
                            <h4 style="color: #ac0101;"><?php echo $this->lang->line('pcontent2'); ?><span></span> </h4>
                            <br>
                            <br>
                            <br>
                            <ul style="text-align: right;">


<li> <h4 style="color: #555555;">
<?php echo $this->lang->line('content3'); ?>
</h4></li>                        </ul>
                             

                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="premium-button">
                            
                            <a class="default-button-btn" href="#"><?php echo $this->lang->line('pcontent1'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Seo premium feature End Here -->





         <!-- Counter section Start Here -->
     <!--   <div class="counter-area-two">
            <div class="container">
                <div class="row auto-clear">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="counter-box mb30-xs">
                            <div class="single-counter">
                                <a href="#" class="pull-left">
                                    <img src="img/3.png" >
                                </a>
                                <div class="media-body">
                                    <h2 class="about-counter" data-num="3498">3498</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="counter-box mb30-xs">
                            <div class="single-counter">
                                <a href="#" class="pull-left">
                                    <img src="img/2.png" >
                                </a>
                                <div class="media-body">
                                    <h2 class="about-counter" data-num="3498">3498</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="counter-box mb30-xs">
                            <div class="single-counter">
                                <a href="#" class="pull-left">
                                    <img src="img/1.png" >
                                </a>
                                <div class="media-body">
                                    <h2 class="about-counter" data-num="3498">3498</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="counter-box mb30-xs">
                            <div class="single-counter">
                                
                                <div class="media-body">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         Counter section End Here -->


        <section id="counter-stats" >
    <div class="container">
        <div class="row">

            <div class="col-lg-3 stats">
                <i class="fa fa-code" aria-hidden="true"></i>
                <div class="counting" data-count="900000">0</div>
                <h5 style="color: #ffffff;"><?php echo $this->lang->line('c1'); ?>
</h5>
            </div>

            <div class="col-lg-3 stats">
                <i class="fa fa-check" aria-hidden="true"></i>
                <div class="counting" data-count="280">0</div>
                <h5 style="color: #ffffff;"><?php echo $this->lang->line('c2'); ?></h5>
            </div>

            <div class="col-lg-3 stats">
                <i class="fa fa-user" aria-hidden="true"></i>
                <div class="counting" data-count="75">0</div>
                <h5 style="color: #ffffff;"><?php echo $this->lang->line('c3'); ?>
</h5>
            </div>

            <div class="col-lg-3 stats">
                <i class="fa fa-coffee" aria-hidden="true"></i>
                <div class="counting" data-count="999">0</div>
                <h5 style="color: #ffffff;"><?php echo $this->lang->line('c4'); ?></h5>
            </div>


        </div>
        <!-- end row -->
    </div>
    <!-- end container -->

</section>

<!-- end cont stats -->
        <br>
        <br>
        
 <div class="container">
                    <div class="title-section">
                    <h2 style="color: #ac0101;"><?php echo $this->lang->line('title_contact'); ?>
 </h2>
                    
                </div>
    <div class="row">
        <div class="col-md-12">
                <div id="Carousel" class="carousel slide">
                 
                <ol class="carousel-indicators">
                    <li data-target="#Carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#Carousel" data-slide-to="1"></li>
                    <li data-target="#Carousel" data-slide-to="2"></li>
                </ol>
                 
                <!-- Carousel items -->
                <div class="carousel-inner">
                    
                <div class="item active">
                    <div class="row">
                      <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/09.png" alt="Image" style="max-width:100%;"></a></div>
                      <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/10.png" alt="Image" style="max-width:100%;"></a></div>
                      <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/11.png" alt="Image" style="max-width:100%;"></a></div>
                      <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/12.png" alt="Image" style="max-width:100%;"></a></div>
                    </div><!--.row-->
                </div><!--.item-->
                 
                <div class="item">
                    <div class="row">
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/13.png" alt="Image" style="max-width:100%;"></a></div>
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/07.png" alt="Image" style="max-width:100%;"></a></div>
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/08.png" alt="Image" style="max-width:100%;"></a></div>
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/09.png" alt="Image" style="max-width:100%;"></a></div>
                    </div><!--.row-->
                </div><!--.item-->
                 
                <div class="item">
                    <div class="row">
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/09.png" alt="Image" style="max-width:100%;"></a></div>
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/10.png" alt="Image" style="max-width:100%;"></a></div>
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/11.png" alt="Image" style="max-width:100%;"></a></div>
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/12.png" alt="Image" style="max-width:100%;"></a></div>
                    </div><!--.row-->
                </div><!--.item-->
                 
                </div><!--.carousel-inner-->
                  <a data-slide="prev" href="#Carousel" class="left carousel-control">‹</a>
                  <a data-slide="next" href="#Carousel" class="right carousel-control">›</a>
                </div><!--.Carousel-->
                 
        </div>
    </div>
</div><!--.container-->


<br>
<br>


 



<!-- Contact page Start Here -->
        <div id="contact" class="contact-page-area padding-top-bottom">
            <div class="container">
                <div class="row">
                    <!-- Main body Start Here -->
                    <div class="body-content">
                        <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                            <form class="contact-form2"  method="POST" action="" >
                                <fieldset>
                                    <!-- Form Name -->
                                    <legend style="color: #ac0101;"><?php echo $this->lang->line('contactus'); ?></legend>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <input id="form-name" name="name" placeholder="<?php echo $this->lang->line('name'); ?>" class="form-control" type="text" data-error="Name field is required" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <input id="form-email" name="email" placeholder="<?php echo $this->lang->line('email'); ?>" class="form-control" type="text" data-error="E-mail field is required" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <input id="form-subject" name="subject" placeholder="<?php echo $this->lang->line('sujet'); ?>" class="form-control" type="text" data-error="Subject field is required" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <input id="form-phone" name="mobile" placeholder="<?php echo $this->lang->line('phone'); ?>" class="form-control" type="text" data-error="Phone field is required" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <!-- Textarea -->
                                            <div class="form-group">
                                                <textarea class="textarea form-control" rows="4" id="form-message" name="message" placeholder="<?php echo $this->lang->line('message'); ?>"data-error="Message field is required" required></textarea>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <!-- Button -->
                                            <div class="form-group button-group">
                                                <button type="submit" name="submit"  style="background-color: #ac0101;"><?php echo $this->lang->line('send'); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                             <?php echo $error; ?>

                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                          
                            <div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=bloc%20C%2C%2075%2C%20avenue%20khairedine%20pacha%20centre%20pacha%2C%20Tunis%201073&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{position:relative;text-align:right;height:326px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:326px;width:100%;}</style></div>
                            
                        </div>
                    </div>
                    <!-- Main body End Here -->
                </div>
            </div>
        </div>
        <!-- Contact page End Here -->



               

        <!-- Footer section Start Here -->
                                                                        <?php if($this->session->userdata('site_lang')=='arabic'){ ?>

        <div class="footer-section-area padding-top-bottom">
            <div class="container">
                <div class="row">
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            
                            <img src="<?php echo base_url() ?>assets/img/softifiblanc.png">
                        </div>
                        <div class="information">
<p style="color: white;">مؤسسة سفتيفي تعد إحدى الشركات الرائدة في مجال تطوير البرمجيات وتطبيقات الويب والمواقع الإلكترونية، فنحن ننتج حلول تقنية لقطاعات أعمال مختلفة تتميز بواجهة مهنية مرنة وسهلة الاستخدام</p>
                            
                        </div>
                        </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>خدماتنا </h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">المتاجر الالكترونية</a></li>
                                <li><a href="#">تطبيقات الجوال</a></li>
                                <li><a href="#">تصميم المواقع</a></li>
                                <li><a href="#">تحسين محركات البحث</a></li>
                                <li><a href="#">التسويق الالكتروني</a></li>
                                <li><a href="#">الاستضافة</a></li>
                            </ul>
                        </div>
                    </div>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>روابط هامة
                        </h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">                                    الرئيسية
</a></li>
                                <li><a href="#">                                    من نحن
</a></li>
                                <li><a href="#">                                    خدماتنا
</a></li>
                                <li><a href="#">                                      أطلب السعر
</a></li>
                                
                            </ul>
                        </div>
                    </div>
                    

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>القائمة البريدية</h3>
                            <p style="color: #ffffff">تابع الأخبار و العروض أولا بأول
</p>

                        </div>
                        <div class="get-quote">
                            <form id="getQuoteForm" role="form">
                                <fieldset>
                                   
                                    <div class="form-group">
                                        <input id="quote-email" name="email" placeholder="البريد الالكتروني " class="form-control" type="text" data-error="الرجاء إدخال بريدك الالكتروني " required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    
                                    <div class="form-group send-button">
                                        <button type="submit" class="ghost-btn btn-send">إشترك الآن</button>
                                    </div>
                                    <div class="form-response"></div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                                        
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer section End Here -->
        <!-- Copyright section Start Here -->
        <div class="copy-right-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="copy-right">
                            <p>كل الحقوق محفوظة مؤسسة سفتيفي ©2020

</a></p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Copyright section End Here -->
    </div>
                                                                                      <?php }elseif($this->session->userdata('site_lang')=='english'){ ?>
                                                                                                <div class="footer-section-area padding-top-bottom">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>Newsletter</h3>
                            <p style="color: #ffffff">Keep up with the news and offers 
</p>

                        </div>
                        <div class="get-quote">
                            <form id="getQuoteForm" role="form">
                                <fieldset>
                                   
                                    <div class="form-group">
                                        <input id="quote-email" name="email" placeholder="Email Adress" class="form-control" type="text" data-error="الرجاء إدخال بريدك الالكتروني " required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    
                                    <div class="form-group send-button">
                                        <button type="submit" class="ghost-btn btn-send">Send</button>
                                    </div>
                                    <div class="form-response"></div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>Our Services </h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">E-commerce</a></li>
                                <li><a href="#">Mobile Applications</a></li>
                                <li><a href="#">Website Development</a></li>
                                <li><a href="#">SEO Optimisation</a></li>
                                <li><a href="#">Digital Marketing</a></li>
                                <li><a href="#">Web Hosting</a></li>
                            </ul>
                        </div>
                    </div>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>Important Links
                        </h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">                                    Home
</a></li>
                                <li><a href="#">                                    About Us
</a></li>
                                <li><a href="#">                                    Services
</a></li>
                                <li><a href="#">                                     Portfolio
</a></li>
                                
                            </ul>
                        </div>
                    </div>
                    

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            
                            <img src="<?php echo base_url() ?>assets/img/softifiblanc.png">
                        </div>
                        <div class="information">
<p style="color: white;">We are an innovative and dynamic global digital agency focused on media with outstanding experience in web and mobile solutions, web marketing strategies and Cloud hosting solutions.
</p>
                            
                        </div>
                        </div>            
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer section End Here -->
        <!-- Copyright section Start Here -->
        <div class="copy-right-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="copy-right">
                            <p>All Rights Reserved Softifi ©2020

</a></p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Copyright section End Here -->
    </div>
     <?php }elseif($this->session->userdata('site_lang')=='french'){ ?>
                                                                                                <div class="footer-section-area padding-top-bottom">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3><?php echo $this->lang->line('newsletter'); ?></h3>
                            <p style="color: #ffffff"><?php echo $this->lang->line('pnewsletter'); ?>
</p>

                        </div>
                        <div class="get-quote">
                            <form id="getQuoteForm" role="form">
                                <fieldset>
                                   
                                    <div class="form-group">
                                        <input id="quote-email" name="email" placeholder="<?php echo $this->lang->line('email'); ?>" class="form-control" type="text" data-error="الرجاء إدخال بريدك الالكتروني " required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    
                                    <div class="form-group send-button">
                                        <button type="submit" class="ghost-btn btn-send"><?php echo $this->lang->line('send'); ?></button>
                                    </div>
                                    <div class="form-response"></div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3><?php echo $this->lang->line('services'); ?></h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">E-commerce</a></li>
                                <li><a href="#">Applications Mobiles</a></li>
                                <li><a href="#">Développement web</a></li>
                                <li><a href="#">Réferencement SEO</a></li>
                                <li><a href="#">Marketing Digital</a></li>
                                <li><a href="#">Hébergement Web</a></li>
                            </ul>
                        </div>
                    </div>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3><?php echo $this->lang->line('lien'); ?>
                        </h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">                                    Accueil
</a></li>
                                <li><a href="#">                                    A Propos
</a></li>
                                <li><a href="#">                                    Services
</a></li>
                                <li><a href="#">                                      Realisations
</a></li>
                                
                            </ul>
                        </div>
                    </div>
                    

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            
                            <img src="<?php echo base_url() ?>assets/img/softifiblanc.png">
                        </div>
                        <div class="information">
<p style="color: white;">Softifi est une agence de création de site internet qui est spécialisée dans la création de site web.Notre agence web en Tunisie met à votre disposition une équipe d’experts et de développeurs web au service de votre performance.
</p>
                            
                        </div>
                        </div>            
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer section End Here -->
        <!-- Copyright section Start Here -->
        <div class="copy-right-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="copy-right">
                            <p>Tous droits réservés Softifi ©2020

</a></p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Copyright section End Here -->
    </div>
                                                  <?php } else{ ?>
                                                                                                                                                    <div class="footer-section-area padding-top-bottom">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3><?php echo $this->lang->line('newsletter'); ?></h3>
                            <p style="color: #ffffff"><?php echo $this->lang->line('pnewsletter'); ?>
</p>

                        </div>
                        <div class="get-quote">
                            <form id="getQuoteForm" role="form">
                                <fieldset>
                                   
                                    <div class="form-group">
                                        <input id="quote-email" name="email" placeholder="<?php echo $this->lang->line('email'); ?>" class="form-control" type="text" data-error="الرجاء إدخال بريدك الالكتروني " required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    
                                    <div class="form-group send-button">
                                        <button type="submit" class="ghost-btn btn-send"><?php echo $this->lang->line('send'); ?></button>
                                    </div>
                                    <div class="form-response"></div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3><?php echo $this->lang->line('services'); ?></h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">E-commerce</a></li>
                                <li><a href="#">Applications Mobiles</a></li>
                                <li><a href="#">Développement web</a></li>
                                <li><a href="#">Réferencement SEO</a></li>
                                <li><a href="#">Marketing Digital</a></li>
                                <li><a href="#">Hébergement Web</a></li>
                            </ul>
                        </div>
                    </div>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3><?php echo $this->lang->line('lien'); ?>
                        </h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">                                    Accueil
</a></li>
                                <li><a href="#">                                    A Propos
</a></li>
                                <li><a href="#">                                    Services
</a></li>
                                <li><a href="#">                                      Realisations
</a></li>
                                
                            </ul>
                        </div>
                    </div>
                    

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            
                            <img src="<?php echo base_url() ?>assets/img/softifiblanc.png">
                        </div>
                        <div class="information">
<p style="color: white;">Softifi est une agence de création de site internet qui est spécialisée dans la création de site web.Notre agence web en Tunisie met à votre disposition une équipe d’experts et de développeurs web au service de votre performance.
</p>
                            
                        </div>
                        </div>            
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer section End Here -->
        <!-- Copyright section Start Here -->
        <div class="copy-right-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="copy-right">
                            <p>Tous droits réservés Softifi ©2020

</a></p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Copyright section End Here -->
    </div>

<?php } ?>


    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <!-- jquery-->
    <script src="<?php echo base_url() ?>assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <!-- Plugins js -->
    <script src="<?php echo base_url() ?>assets/js/plugins.js" type="text/javascript"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Meanmenu Js -->
    <script src="<?php echo base_url() ?>assets/js/jquery.meanmenu.min.js" type="text/javascript"></script>
    <!-- Counter Js -->
    <script src="<?php echo base_url() ?>assets/js/jquery.counterup.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/js/waypoints.min.js" type="text/javascript"></script>
    <!-- WOW JS -->
    <script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>
    <!-- Nivo slider js -->
    <script src="<?php echo base_url() ?>assets/vendor/slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/vendor/slider/home.js" type="text/javascript"></script>
    <!-- Owl Cauosel JS -->
    <script src="<?php echo base_url() ?>assets/vendor/OwlCarousel/owl.carousel.min.js" type="text/javascript"></script>
    <!-- Srollup js -->
    <script src="<?php echo base_url() ?>assets/js/jquery.scrollUp.min.js" type="text/javascript"></script>
    <!-- Google Map js -->
    <script src="<?php echo base_url() ?>assets/https://maps.googleapis.com/maps/api/js?key=AIzaSyBgREM8KO8hjfbOC0R_btBhQsEQsnpzFGQ"></script>
    <!-- Validator js -->
    <script src="<?php echo base_url() ?>assets/js/validator.min.js" type="text/javascript"></script>
    <!-- One Page menu action JS -->
    <script src="<?php echo base_url() ?>assets/js/jquery.nav.js" type="text/javascript"></script>
    <!-- Custom Js -->
    <script src="<?php echo base_url() ?>assets/js/main.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
    $('#Carousel').carousel({
        interval: 5000
    })
});

        $(document).ready(function(){

    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');
        
        if(value == "all")
        {
            //$('.filter').removeClass('hidden');
            $('.filter').show('1000');
        }
        else
        {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');
            
        }
    });
    
    if ($(".filter-button").removeClass("active")) {
$(this).removeClass("active");
}
$(this).addClass("active");

});

        $(function(){
    $('.categories a').click(function(e){
        $('.categories ul li').removeClass('active');
        $(this).parent('li').addClass('active');
        var itemSelected = $(this).attr('data-filter');
        $('.portfolio-item').each(function(){
            if (itemSelected == '*'){
                $(this).removeClass('filtered').removeClass('selected');
                return;
            } else if($(this).is(itemSelected)){
                $(this).removeClass('filtered').addClass('selected');
            } else{
                $(this).removeClass('selected').addClass('filtered');
            }
        });
    });
});

        // number count for stats, using jQuery animate

$('.counting').each(function() {
  var $this = $(this),
      countTo = $this.attr('data-count');
  
  $({ countNum: $this.text()}).animate({
    countNum: countTo
  },

  {

    duration: 3000,
    easing:'linear',
    step: function() {
      $this.text(Math.floor(this.countNum));
    },
    complete: function() {
      $this.text(this.countNum);
      //alert('finished');
    }

  });  
  

});
    </script>
</body>


</html>
