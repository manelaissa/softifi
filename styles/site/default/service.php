<!doctype html>
<html   lang="en">


<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Softifi Agency</title>
    <meta name="description" content="<?php echo config('meta_description') ?>">
        <meta name="Keywords" content="<?php echo config('meta_keywords') ?>">
        <link rel="shortcut icon" href="<?php echo config('favicon') ?>" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() ?>assets/img/softifi.jpg">
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/meanmenu.min.css">
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/normalize.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/animate.min.css">
    <!-- Font-flat CSS-->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/fonts/flaticon.css">
    <!-- Owl Caousel CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/OwlCarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/OwlCarousel/owl.theme.default.min.css">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/slider/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/slider/css/preview.css" type="text/css" media="screen" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
    <script src="<?php echo base_url() ?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

<style type="text/css">
                        



</style>
</head>

<body>
   
    <div class="wraper">
        <header id="home">
            <div class="header-two">
                <div class="header-top-area" id="sticker">
                    <div class="container">
                        <div class="row">
                                                                <?php if($this->session->userdata('site_lang')=='arabic'){ ?>

                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="float: right;">
                                <div class="logo-area">
                                    <a href="<?php echo base_url() ?>"><img class="img-responsive" style="float: right;" src="<?php echo base_url() ?>assets/img/softifilogo.jpg" alt="logo"></a>
                                </div>
                            </div>
                                                                                  <?php }elseif($this->session->userdata('site_lang')=='english'){ ?>
                                      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="float: left;">
                                <div class="logo-area">                                              
                                    <a href="<?php echo base_url() ?>"><img class="img-responsive" style="float: left;" src="<?php echo base_url() ?>assets/img/softifilogo.jpg" alt="logo"></a>
                                </div>
                            </div>
                          <?php }elseif($this->session->userdata('site_lang')=='french'){ ?>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="float: left;">
                                <div class="logo-area">                                              
                                    <a href="<?php echo base_url() ?>"><img class="img-responsive" style="float: left;" src="<?php echo base_url() ?>assets/img/softifilogo.jpg" alt="logo"></a>
                                </div>
                            </div>

                                              <?php } else{ ?>
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="float: left;">
                                <div class="logo-area"> 
                                    <a href="<?php echo base_url() ?>"><img class="img-responsive" style="float: left;" src="<?php echo base_url() ?>assets/img/softifilogo.jpg" alt="logo"></a>
</div>
</div>
                            <?php } ?>

                            
                            <?php if($this->session->userdata('site_lang')=='arabic'){ ?>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <div class="main-menu">
                                    <nav>
                                        <ul id="navOnePage">

                                           <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/english"><?php echo $this->lang->line('english'); ?></a>
                                            <ul>
  <li ><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/french"><?php echo $this->lang->line('french'); ?></a></li>

 </ul>
</li>
                                           </li>
                                             <li><a href="#contact"><?php echo $this->lang->line('contactus'); ?></a></li>
                                           
                                            <li><a href="#projects"><?php echo $this->lang->line('projects'); ?></a></li>
                                            <li><a href="#service"><?php echo $this->lang->line('services'); ?></a></li>

                                             <li><a href="#about"><?php echo $this->lang->line('aboutus'); ?></a></li>
                                             <li>
                                                <a href="index.php"><?php echo $this->lang->line('home'); ?> </a>
                                               
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                                              <?php }elseif($this->session->userdata('site_lang')=='english'){ ?>
 <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <div class="main-menu">
                                    <nav>
                                        <ul id="navOnePage">
<li>
                                                <a href="index.php"><?php echo $this->lang->line('home'); ?> </a>
                                               
                                            </li>
                                            <li><a href="#about"><?php echo $this->lang->line('aboutus'); ?></a></li>
                                             <li><a href="#service"><?php echo $this->lang->line('services'); ?></a></li>
                                              <li><a href="#projects"><?php echo $this->lang->line('projects'); ?> </a></li>
                                             <li><a href="#contact"><?php echo $this->lang->line('contactus'); ?></a></li>
                                            <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/french"><?php echo $this->lang->line('french'); ?>
</a>
<ul>
                                            
  <li ><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/arabic"><?php echo $this->lang->line('arabic'); ?></a></li>

 </ul>
</li>




                                            
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                          <?php }elseif($this->session->userdata('site_lang')=='french'){ ?>
                             <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <div class="main-menu">
                                    <nav>
                                        <ul id="navOnePage">
<li>
                                                <a href="index.php"><?php echo $this->lang->line('home'); ?> </a>
                                               
                                            </li>
                                            <li><a href="#about"><?php echo $this->lang->line('aboutus'); ?></a></li>
                                             <li><a href="#service"><?php echo $this->lang->line('services'); ?></a></li>
                                              <li><a href="#projects"><?php echo $this->lang->line('projects'); ?></a></li>
                                             <li><a href="#contact"><?php echo $this->lang->line('contactus'); ?></a></li>
                                              <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/arabic"><?php echo $this->lang->line('arabic'); ?>
</a>
<ul>
                                            
  <li ><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/english"><?php echo $this->lang->line('english'); ?></a></li>

 </ul>
</li>







                                            
                                        </ul>
                                    </nav>
                                </div>
                            </div>

                                              <?php } else{ ?>
                          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <div class="main-menu">
                                    <nav>
                                        <ul id="navOnePage">
<li>
                                                <a href="index.php"><?php echo $this->lang->line('home'); ?> </a>
                                               
                                            </li>
                                            <li><a href="#about"><?php echo $this->lang->line('aboutus'); ?></a></li>
                                             <li><a href="#service"><?php echo $this->lang->line('services'); ?></a></li>
                                              <li><a href="#projects"><?php echo $this->lang->line('projects'); ?></a></li>
                                             <li><a href="#contact"><?php echo $this->lang->line('contactus'); ?></a></li>
                                            <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/arabic"><?php echo $this->lang->line('arabic'); ?>
</a>
<ul>
                                            
  <li ><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/english"><?php echo $this->lang->line('english'); ?></a></li>

 </ul>
</li>

                                            
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
                                                                            <?php if($this->session->userdata('site_lang')=='arabic'){ ?>

            <!-- mobile-menu-area start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                        <li>
                                                <a href="index.php">الرئيسية </a>
                                               
                                            </li>
                                            <li><a href="#about">من نحن  </a></li>

                                            <li><a href="#service">خدماتنا</a></li>
                                            <li><a href="#projects">معرض أعمالنا </a></li>
                                            <li><a href="#contact">اتصل بنا</a></li>
                                             <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/english"><?php echo $this->lang->line('english'); ?></a></li>
                                        </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                                                                                              <?php }elseif($this->session->userdata('site_lang')=='english'){ ?>
                                                                                                         <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                     <li>
                                                <a href="index.php"><?php echo $this->lang->line('home'); ?> </a>
                                               
                                            </li>
                                            <li><a href="#about"><?php echo $this->lang->line('aboutus'); ?></a></li>
                                             <li><a href="#service"><?php echo $this->lang->line('services'); ?></a></li>
                                              <li><a href="#projects">Our Projects </a></li>
                                             <li><a href="#contact"><?php echo $this->lang->line('contactus'); ?></a></li>
                                            <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/arabic"><?php echo $this->lang->line('arabic'); ?>
</a></li>
                                        </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php }elseif($this->session->userdata('site_lang')=='french'){ ?>
                                                                                                                       <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                     <li>
                                                <a href="index.php"><?php echo $this->lang->line('home'); ?> </a>
                                               
                                            </li>
                                            <li><a href="#about"><?php echo $this->lang->line('aboutus'); ?></a></li>
                                             <li><a href="#service"><?php echo $this->lang->line('services'); ?></a></li>
                                              <li><a href="#projects"><?php echo $this->lang->line('projects'); ?> </a></li>
                                             <li><a href="#contact"><?php echo $this->lang->line('contactus'); ?></a></li>
                                            <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/arabic"><?php echo $this->lang->line('arabic'); ?>
</a></li>
                                        </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                                                          <?php } else{ ?>
                                                                  <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                     <li>
                                                <a href="index.php"><?php echo $this->lang->line('home'); ?> </a>
                                               
                                            </li>
                                            <li><a href="#about"><?php echo $this->lang->line('aboutus'); ?></a></li>
                                             <li><a href="#service"><?php echo $this->lang->line('services'); ?></a></li>
                                              <li><a href="#projects"><?php echo $this->lang->line('projects'); ?></a></li>
                                             <li><a href="#contact"><?php echo $this->lang->line('contactus'); ?></a></li>
                                            <li><a href="<?php echo base_url()?>index.php?/LanguageSwitcher/switchlang/arabic"><?php echo $this->lang->line('arabic'); ?>
</a></li>
                                        </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>


            <!-- mobile-menu-area end -->
        </header>
        <!-- Header Area End Here -->




               

        <?php if($this->session->userdata('site_lang')=='arabic'){ ?>

        <div class="footer-section-area padding-top-bottom">
            <div class="container">
                <div class="row">
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            
                            <img src="<?php echo base_url() ?>assets/img/softifiblanc.png">
                        </div>
                        <div class="information">
<p style="color: white;">مؤسسة سفتيفي تعد إحدى الشركات الرائدة في مجال تطوير البرمجيات وتطبيقات الويب والمواقع الإلكترونية، فنحن ننتج حلول تقنية لقطاعات أعمال مختلفة تتميز بواجهة مهنية مرنة وسهلة الاستخدام</p>
                            
                        </div>
                        </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>خدماتنا </h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">المتاجر الالكترونية</a></li>
                                <li><a href="#">تطبيقات الجوال</a></li>
                                <li><a href="#">تصميم المواقع</a></li>
                                <li><a href="#">تحسين محركات البحث</a></li>
                                <li><a href="#">التسويق الالكتروني</a></li>
                                <li><a href="#">الاستضافة</a></li>
                            </ul>
                        </div>
                    </div>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>روابط هامة
                        </h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">                                    الرئيسية
</a></li>
                                <li><a href="#">                                    من نحن
</a></li>
                                <li><a href="#">                                    خدماتنا
</a></li>
                                <li><a href="#">                                      أطلب السعر
</a></li>
                                
                            </ul>
                        </div>
                    </div>
                    

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>القائمة البريدية</h3>
                            <p style="color: #ffffff">تابع الأخبار و العروض أولا بأول
</p>

                        </div>
                        <div class="get-quote">
                            <form id="getQuoteForm" role="form">
                                <fieldset>
                                   
                                    <div class="form-group">
                                        <input id="quote-email" name="email" placeholder="البريد الالكتروني " class="form-control" type="text" data-error="الرجاء إدخال بريدك الالكتروني " required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    
                                    <div class="form-group send-button">
                                        <button type="submit" class="ghost-btn btn-send">إشترك الآن</button>
                                    </div>
                                    <div class="form-response"></div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                                        
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer section End Here -->
        <!-- Copyright section Start Here -->
        <div class="copy-right-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="copy-right">
                            <p>كل الحقوق محفوظة مؤسسة سفتيفي ©2020

</a></p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Copyright section End Here -->
    </div>
                                                                                      <?php }elseif($this->session->userdata('site_lang')=='english'){ ?>
                                                                                                <div class="footer-section-area padding-top-bottom">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>Newsletter</h3>
                            <p style="color: #ffffff">Keep up with the news and offers 
</p>

                        </div>
                        <div class="get-quote">
                            <form id="getQuoteForm" role="form">
                                <fieldset>
                                   
                                    <div class="form-group">
                                        <input id="quote-email" name="email" placeholder="Email Adress" class="form-control" type="text" data-error="الرجاء إدخال بريدك الالكتروني " required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    
                                    <div class="form-group send-button">
                                        <button type="submit" class="ghost-btn btn-send">Send</button>
                                    </div>
                                    <div class="form-response"></div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>Our Services </h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">E-commerce</a></li>
                                <li><a href="#">Mobile Applications</a></li>
                                <li><a href="#">Website Development</a></li>
                                <li><a href="#">SEO Optimisation</a></li>
                                <li><a href="#">Digital Marketing</a></li>
                                <li><a href="#">Web Hosting</a></li>
                            </ul>
                        </div>
                    </div>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>Important Links
                        </h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">                                    Home
</a></li>
                                <li><a href="#">                                    About Us
</a></li>
                                <li><a href="#">                                    Services
</a></li>
                                <li><a href="#">                                     Portfolio
</a></li>
                                
                            </ul>
                        </div>
                    </div>
                    

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            
                            <img src="<?php echo base_url() ?>assets/img/softifiblanc.png">
                        </div>
                        <div class="information">
<p style="color: white;">We are an innovative and dynamic global digital agency focused on media with outstanding experience in web and mobile solutions, web marketing strategies and Cloud hosting solutions.
</p>
                            
                        </div>
                        </div>            
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer section End Here -->
        <!-- Copyright section Start Here -->
        <div class="copy-right-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="copy-right">
                            <p>All Rights Reserved Softifi ©2020

</a></p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Copyright section End Here -->
    </div>
     <?php }elseif($this->session->userdata('site_lang')=='french'){ ?>
                                                                                                <div class="footer-section-area padding-top-bottom">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3><?php echo $this->lang->line('newsletter'); ?></h3>
                            <p style="color: #ffffff"><?php echo $this->lang->line('pnewsletter'); ?>
</p>

                        </div>
                        <div class="get-quote">
                            <form id="getQuoteForm" role="form">
                                <fieldset>
                                   
                                    <div class="form-group">
                                        <input id="quote-email" name="email" placeholder="<?php echo $this->lang->line('email'); ?>" class="form-control" type="text" data-error="الرجاء إدخال بريدك الالكتروني " required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    
                                    <div class="form-group send-button">
                                        <button type="submit" class="ghost-btn btn-send"><?php echo $this->lang->line('send'); ?></button>
                                    </div>
                                    <div class="form-response"></div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3><?php echo $this->lang->line('services'); ?></h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">E-commerce</a></li>
                                <li><a href="#">Applications Mobiles</a></li>
                                <li><a href="#">Développement web</a></li>
                                <li><a href="#">Réferencement SEO</a></li>
                                <li><a href="#">Marketing Digital</a></li>
                                <li><a href="#">Hébergement Web</a></li>
                            </ul>
                        </div>
                    </div>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3><?php echo $this->lang->line('lien'); ?>
                        </h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">                                    Accueil
</a></li>
                                <li><a href="#">                                    A Propos
</a></li>
                                <li><a href="#">                                    Services
</a></li>
                                <li><a href="#">                                      Realisations
</a></li>
                                
                            </ul>
                        </div>
                    </div>
                    

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            
                            <img src="<?php echo base_url() ?>assets/img/softifiblanc.png">
                        </div>
                        <div class="information">
<p style="color: white;">Softifi est une agence de création de site internet qui est spécialisée dans la création de site web.Notre agence web en Tunisie met à votre disposition une équipe d’experts et de développeurs web au service de votre performance.
</p>
                            
                        </div>
                        </div>            
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer section End Here -->
        <!-- Copyright section Start Here -->
        <div class="copy-right-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="copy-right">
                            <p>Tous droits réservés Softifi ©2020

</a></p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Copyright section End Here -->
    </div>
                                                  <?php } else{ ?>
                                                                                                                                                    <div class="footer-section-area padding-top-bottom">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3><?php echo $this->lang->line('newsletter'); ?></h3>
                            <p style="color: #ffffff"><?php echo $this->lang->line('pnewsletter'); ?>
</p>

                        </div>
                        <div class="get-quote">
                            <form id="getQuoteForm" role="form">
                                <fieldset>
                                   
                                    <div class="form-group">
                                        <input id="quote-email" name="email" placeholder="<?php echo $this->lang->line('email'); ?>" class="form-control" type="text" data-error="الرجاء إدخال بريدك الالكتروني " required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    
                                    <div class="form-group send-button">
                                        <button type="submit" class="ghost-btn btn-send"><?php echo $this->lang->line('send'); ?></button>
                                    </div>
                                    <div class="form-response"></div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3><?php echo $this->lang->line('services'); ?></h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">E-commerce</a></li>
                                <li><a href="#">Applications Mobiles</a></li>
                                <li><a href="#">Développement web</a></li>
                                <li><a href="#">Réferencement SEO</a></li>
                                <li><a href="#">Marketing Digital</a></li>
                                <li><a href="#">Hébergement Web</a></li>
                            </ul>
                        </div>
                    </div>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3><?php echo $this->lang->line('lien'); ?>
                        </h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">                                    Accueil
</a></li>
                                <li><a href="#">                                    A Propos
</a></li>
                                <li><a href="#">                                    Services
</a></li>
                                <li><a href="#">                                      Realisations
</a></li>
                                
                            </ul>
                        </div>
                    </div>
                    

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            
                            <img src="<?php echo base_url() ?>assets/img/softifiblanc.png">
                        </div>
                        <div class="information">
<p style="color: white;">Softifi est une agence de création de site internet qui est spécialisée dans la création de site web.Notre agence web en Tunisie met à votre disposition une équipe d’experts et de développeurs web au service de votre performance.
</p>
                            
                        </div>
                        </div>            
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer section End Here -->
        <!-- Copyright section Start Here -->
        <div class="copy-right-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="copy-right">
                            <p>Tous droits réservés Softifi ©2020

</a></p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Copyright section End Here -->
    </div>

<?php } ?>


    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <!-- jquery-->
    <script src="<?php echo base_url() ?>assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <!-- Plugins js -->
    <script src="<?php echo base_url() ?>assets/js/plugins.js" type="text/javascript"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Meanmenu Js -->
    <script src="<?php echo base_url() ?>assets/js/jquery.meanmenu.min.js" type="text/javascript"></script>
    <!-- Counter Js -->
    <script src="<?php echo base_url() ?>assets/js/jquery.counterup.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/js/waypoints.min.js" type="text/javascript"></script>
    <!-- WOW JS -->
    <script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>
    <!-- Nivo slider js -->
    <script src="<?php echo base_url() ?>assets/vendor/slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/vendor/slider/home.js" type="text/javascript"></script>
    <!-- Owl Cauosel JS -->
    <script src="<?php echo base_url() ?>assets/vendor/OwlCarousel/owl.carousel.min.js" type="text/javascript"></script>
    <!-- Srollup js -->
    <script src="<?php echo base_url() ?>assets/js/jquery.scrollUp.min.js" type="text/javascript"></script>
    <!-- Google Map js -->
    <script src="<?php echo base_url() ?>assets/https://maps.googleapis.com/maps/api/js?key=AIzaSyBgREM8KO8hjfbOC0R_btBhQsEQsnpzFGQ"></script>
    <!-- Validator js -->
    <script src="<?php echo base_url() ?>assets/js/validator.min.js" type="text/javascript"></script>
    <!-- One Page menu action JS -->
    <script src="<?php echo base_url() ?>assets/js/jquery.nav.js" type="text/javascript"></script>
    <!-- Custom Js -->
    <script src="<?php echo base_url() ?>assets/js/main.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
    $('#Carousel').carousel({
        interval: 5000
    })
});

        $(document).ready(function(){

    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');
        
        if(value == "all")
        {
            //$('.filter').removeClass('hidden');
            $('.filter').show('1000');
        }
        else
        {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');
            
        }
    });
    
    if ($(".filter-button").removeClass("active")) {
$(this).removeClass("active");
}
$(this).addClass("active");

});

        $(function(){
    $('.categories a').click(function(e){
        $('.categories ul li').removeClass('active');
        $(this).parent('li').addClass('active');
        var itemSelected = $(this).attr('data-filter');
        $('.portfolio-item').each(function(){
            if (itemSelected == '*'){
                $(this).removeClass('filtered').removeClass('selected');
                return;
            } else if($(this).is(itemSelected)){
                $(this).removeClass('filtered').addClass('selected');
            } else{
                $(this).removeClass('selected').addClass('filtered');
            }
        });
    });
});

        // number count for stats, using jQuery animate

$('.counting').each(function() {
  var $this = $(this),
      countTo = $this.attr('data-count');
  
  $({ countNum: $this.text()}).animate({
    countNum: countTo
  },

  {

    duration: 3000,
    easing:'linear',
    step: function() {
      $this.text(Math.floor(this.countNum));
    },
    complete: function() {
      $this.text(this.countNum);
      //alert('finished');
    }

  });  
  

});
    </script>
</body>


</html>
