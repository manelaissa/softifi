<!doctype html>
<html   lang="en">


<!-- Mirrored from radiustheme.com/demo/html/seoengine/one-page/index5.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 21 Jan 2020 07:52:16 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Softifi Agency</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url() ?>assets/img/softifi.jpg">
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/meanmenu.min.css">
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/normalize.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/animate.min.css">
    <!-- Font-flat CSS-->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/fonts/flaticon.css">
    <!-- Owl Caousel CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/OwlCarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/OwlCarousel/owl.theme.default.min.css">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/slider/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/vendor/slider/css/preview.css" type="text/css" media="screen" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">
    <script src="<?php echo base_url() ?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<style type="text/css">


.single-service:hover
{
    background: #ede8e8;
}

.media-body.content h3:hover
{
    color: #ac0101;
}
    .carousel {
    margin-bottom: 0;
    padding: 0 40px 30px 40px;
}
/* The controlsy */
.carousel-control {
    left: -12px;
    height: 40px;
    width: 40px;
    background: none repeat scroll 0 0 #222222;
    border: 4px solid #FFFFFF;
    border-radius: 23px 23px 23px 23px;
    margin-top: 90px;
}
.carousel-control.right {
    right: -12px;
}
/* The indicators */
.carousel-indicators {
    right: 50%;
    top: auto;{
    background: #46d2a7;
    bottom: -10px;
    margin-right: -19px;
}
/* The colour of the indicators */
.carousel-indicators li {
    background: #cecece;
}
.carousel-indicators .active {
background: #428bca;
}
filter-button button.is-checked 
    color: #ffffff;
}

section#counter-stats {
    justify-content: center;
    margin-top: 100px;
    background-color: #ac0101;
}

.stats {
  text-align: center;
  font-size: 35px;
  font-weight: 700;
  font-family: 'Droid Arabic Kufi', sans-serif;
  color: #ffffff;
}

.stats .fa {
  color: #ffffff;
  font-size: 60px;
}
</style>
</head>

<body>
   
    <div class="wraper">
        <header id="home">
            <div class="header-two">
                <div class="header-top-area" id="sticker">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="logo-area">
                                    <a href="index.php"><img class="img-responsive" src="<?php echo base_url() ?>assets/img/softifilogo.jpg" alt="logo"></a>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <div class="main-menu">
                                    <nav>
                                        <ul id="navOnePage">

                                           <li><a href="#">English</a></li>
                                             <li><a href="#contact">اتصل بنا</a></li>

                                            <li><a href="#pricing-plan">معرض أعمالنا </a></li>
                                            <li><a href="#service">خدماتنا</a></li>

                                             <li><a href="#about">عن الشركة</a></li>
                                             <li>
                                                <a href="index.php">الرئيسية </a>
                                               
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- mobile-menu-area start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                        <li>
                                                <a href="index.php">الرئيسية </a>
                                               
                                            </li>
                                            <li><a href="#about">عن الشركة</a></li>

                                            <li><a href="#service">خدماتنا</a></li>
                                            <li><a href="#pricing-plan">معرض أعمالنا </a></li>
                                            <li><a href="#contact">اتصل بنا</a></li>
                                        </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- mobile-menu-area end -->
        </header>
        <!-- Header Area End Here -->
        <!-- Slider 1 Area Start Here -->

        <div class="slider-default slider-overlay">
            <div class="bend niceties preview-1">
                <div id="ensign-nivoslider-3" class="slides">
                    <img src="<?php echo base_url() ?>assets/img/slider9.png" alt="slider" title="#slider-direction-1" />
                    <img src="<?php echo base_url() ?>assets/img/slider9.png" alt="slider" title="#slider-direction-2" />
                    <img src="<?php echo base_url() ?>assets/img/slider9.png" alt="slider" title="#slider-direction-3" />
                </div>
                <div id="slider-direction-1" class="t-cn slider-direction">
                    <div class="slider-content s-tb slide-1 container">
                        <div class="title-container s-tb-c">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb30-xs">
                        <div class="content-part">
                            <p class="slider-big-text" >نوفر أعلى جودة في تصميم وتطوير مواقع الإنترنت</p>
                            <p>تصميم المواقع، العلامات التجارية للشركات، خدمات التسويق الإلكتروني ومحركات البحث، تهيئة المواقع الإلكترونية للتوافق مع معايير محركات البحث...

</p>
<div class="slider-btn-area" align="center">
                                <a href="#" class="ghost-btn"> </a>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="text-center">
                            <img src="<?php echo base_url() ?>assets/img/softifiblanc.png" alt="what seo"/>
                        </div>
                    </div>
                            
                            
                        </div>
                    </div>
                </div>
                <div id="slider-direction-2" class="t-cn slider-direction">
                    <div class="slider-content s-tb slide-2 container">
                        <div class="title-container s-tb-c">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb30-xs">
                        <div class="content-part">
                            <p class="slider-big-text" >نوفر أعلى جودة في تصميم وتطوير مواقع الإنترنت</p>
                            <p>تصميم المواقع، العلامات التجارية للشركات، خدمات التسويق الإلكتروني ومحركات البحث، تهيئة المواقع الإلكترونية للتوافق مع معايير محركات البحث...

</p>
<div class="slider-btn-area" align="center">
                                <a href="#" class="ghost-btn"> </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="text-center">
                            <img src="<?php echo base_url() ?>assets/img/softifilogo.jpg" alt="what seo">
                        </div>
                    </div>
                            
                        </div>
                    </div>
                </div>
                <div id="slider-direction-3" class="t-cn slider-direction">
                    <div class="slider-content s-tb slide-3 container">
                        <div class="title-container s-tb-c">
                             <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mb30-xs">
                        <div class="content-part">
                            <p class="slider-big-text" >نوفر أعلى جودة في تصميم وتطوير مواقع الإنترنت</p>
                            <p>تصميم المواقع، العلامات التجارية للشركات، خدمات التسويق الإلكتروني ومحركات البحث، تهيئة المواقع الإلكترونية للتوافق مع معايير محركات البحث...

</p>
<div class="slider-btn-area" align="center">
                                <a href="#" class="ghost-btn"> </a>
                            </div>                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="text-center">
                            <img src="<?php echo base_url() ?>assets/img/softifilogo.jpg" alt="what seo">
                        </div>
                    </div>
                            <p class="slider-big-text"></p>
                            <p></p>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Slider 1 Area End Here -->
        <!-- Home Five service Start Here -->
        <div id="service" class="home-five-service padding-top-bottom">
            <div class="container">

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="single-service mb30-xs">
                            <div class="service-img">


                                <img src="<?php echo base_url() ?>assets/img/01.png" width="100" height="70" alt="service" class="img-responsive">
                            </div>
                            <div class="media-body content">
                                <h3>المتاجر الالكترونية</h3>
                                <p>وفرنا لكم باقات تصميم المتاجر التى صممت بعناية و إحترافية و التى تتوافق مع معايير التجارة الإلكترونية الآمنة</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="single-service mb30-xs">
                            <div class="service-img">
                                <img src="<?php echo base_url() ?>assets/img/02.png" width="100" height="70" alt="service" class="img-responsive">
                            </div>
                            <div class="media-body content">
                                <h3>تطبيقات الجوال</h3>
                                <p>نحن نقوم بتطوير تطبيقات للهواتف المحمولة مخصصة على حسب احتياجات العميل</p>
                            </div>
                        </div>
                    </div>
                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="single-service mb30-xs">
                            <div class="service-img">
                                <img src="<?php echo base_url() ?>assets/img/03.png"  width="100" height="70" alt="service" class="img-responsive">
                            </div>
                            <div class="media-body content">
                                <h3>تصميم المواقع </h3>
                                <p>نقدم لكم خدمات تصميم المواقع الالكترونية، الهوية المطبوعة وتصميم واجهات تطبيقات الموبايل.</p>
                            </div>
                        </div>
                    </div>
                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="single-service mb30-xs">
                            <div class="service-img">
                                <img src="<?php echo base_url() ?>assets/img/04.png" width="100" height="70" alt="service" class="img-responsive">
                            </div>
                            <div class="media-body content">
                                <h3>تحسين محركات البحث</h3>
                                <p>تريد بقاء موقعك لفترة طويلة على الويب والحفاظ على قوة الموقع واستمراره؟
</p>
                            </div>
                        </div>
                    </div>
                             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="single-service mb30-xs">
                            <div class="service-img">
                                <img src="<?php echo base_url() ?>assets/img/01.png" width="100" height="70" alt="service" class="img-responsive">
                            </div>
                            <div class="media-body content">
                                <h3>التسويق الالكتروني </h3>
                                <p>لديك منتجات مميزة، و لكن لا تستطيع أن تقنع عملائك بمدى تميز منتجك؟</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="single-service mb30-xs">
                            <div class="service-img">
                                <img src="<?php echo base_url() ?>assets/img/02.png" width="100" height="70" alt="service" class="img-responsive">
                            </div>
                            <div class="media-body content">
                                <h3>الاستضافة</h3>
                                <p>نقدم لكم الاستضافة المشتركة, استضافة مواقع الشركات, استضافة المنتديات, استضافة المواقع الالكترونية.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       

       <div>
        <h4 style="color: #555555; text-align: center;">مؤسسة سفتيفي تعد إحدى شركات الرائدة في مجال تطوير البرمجيات وتطبيقات الويب والمواقع الإلكترونية، فنحن ننتج حلول تقنية لقطاعات أعمال مختلفة تتميز بواجهة مهنية مرنة وسهلة الاستخدام.</h4>
<img src="<?php echo base_url() ?>assets/img/pc.png" style=" width: 40%;display: block;
    margin: 0 auto;" class="img-responsive" alt="what seo">
       </div>









 <section id="counter-stats" >
    <div class="container">
        <div class="row">

            <div class="col-lg-3 stats">
                <h5 style="color: white;">نحن نعمل جاهدا لاصدار باقة من المنتوجات الرقمية فائقة الجودة
</h5>
            </div>

            <div class="col-lg-3 stats">
                
            </div>

            <div class="col-lg-3 stats">
                <div class="slider-btn-area">
                                <a href="#" class="ghost-btn" style="background-color: #ffffff"> ابدأ مشروعك الآن</a>
                            </div>
                        </div>
            </div>

            <div class="col-lg-3 stats">
               
            </div>


        </div>
        <!-- end row -->
    </div>
    <!-- end container -->

</section>









<section id="projects">
         <!-- Recent case studies Start Here -->
        <div id="case" class="recent-case-three-style padding-two-top-bottom position-relative bg-accent">
            <div class="container">
                <div class="title-section">
                    <h2 style="color: #ac0101;">أعمالنا ومشروعاتنا</h2>
                    <h4 style="color: #555555;">مهمتنا توفير أفضل حلول الأعمال الإلكترونية والخدمات المهنية، والمصممة بإحترافية خصيصاً وفق إحتياجات الشركات

</h4>
                    
                </div>
    
    
               
               <div class="container">

        <div class="row">
        <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
        </div>

        <div align="center">
            <button class="btn btn-default filter-button" class="is-checked" data-filter="all">الكل</button>
            <button class="btn btn-default filter-button" data-filter="websites">تطوير المواقع و المحتوى</button>
            <button class="btn btn-default filter-button" data-filter="software">برامج و تطبيقات الكمبيوتر</button>
            <button class="btn btn-default filter-button" data-filter="mobile apps">تطبيقات الأجهزة الذكية</button>
            <button class="btn btn-default filter-button" data-filter="design">التصميم الرقمي
</button>
        </div>
        <br/>

      

            <div class="gallery_product  col-lg-4 col-md-4 col-sm-4 col-xs-6 filter websites">

              <img src="<?php echo base_url() ?>assets/img/image4.jpg" class="img-responsive"> 

            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter websites">
                <img src="<?php echo base_url() ?>assets/img/image4.jpg" class="img-responsive">
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter software">
                <img src="<?php echo base_url() ?>assets/img/image4.jpg" class="img-responsive">
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter design">
                <img src="<?php echo base_url() ?>assets/img/image4.jpg" class="img-responsive">
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter mobile apps">
                <img src="<?php echo base_url() ?>assets/img/image4.jpg" class="img-responsive">
            </div>

            <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter design">
                <img src="<?php echo base_url() ?>assets/img/image4.jpg" class="img-responsive">
            </div>

           
        </div>
    </div>
</section>

            </div>
        </div>
        <!-- Recent case studies End Here -->
        <br>
        <br>


        <!-- Home Two service End Here -->
        <!-- About SEO start Here -->
        <div id="about" class="about-seo-three padding-top-bottom position-relative">
            <div class="container">
                <div class="row">
                        <div class="content-part">
                             <img src="<?php echo base_url() ?>assets/img/softifiblanc.png" style="margin-right: 300px;" alt="what seo" >

                            <h4 style="color: #555555; text-align: center;">مؤسسة سفتيفي تعد إحدى الشركات الرائدة في مجال تطوير البرمجيات وتطبيقات الويب والمواقع الإلكترونية، فنحن ننتج حلول تقنية لقطاعات أعمال مختلفة تتميز بواجهة مهنية مرنة وسهلة الاستخدام</h4>
                            <p></p>
                        </div>
                    </div>
                    
            </div>
        </div>
        <!-- About SEO End Here -->
      






       
        <!-- Seo premium feature Start Here -->
        <div class="premium-feature-three">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="premium-content">
                            <h4 style="color: #ac0101;">مؤسسة سفتيفي هي شركة تطوير تطبيقات الهواتف الذكية وتصميم المواقع الإلكترونية وإدارة المحتوى <span></span> </h4>
                            <br>
                            <br>
                            <br>
                            <ul style="text-align: right;">


<li> <h4 style="color: #555555;">
مواقع الانترنت مازالت هي مركز التجارة الالكترونية الأساسي في منطقتنا العربية ومصدر الخبر والمعلومة الأكثر جماهيرية لذا نحن نحرص على تقديم الإهتمام الأوفر بها من خلال مبرمجين مهرة وادوات تقنية متجددة
</h4></li>                        </ul>
                             

                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="premium-button">
                            <img src="<?php echo base_url() ?>assets/img/mobile-softifi.png" width="500" height="400">
                            <a class="default-button-btn" href="#">ابدأ مشروعك الآن</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Seo premium feature End Here -->





         <!-- Counter section Start Here -->
     <!--   <div class="counter-area-two">
            <div class="container">
                <div class="row auto-clear">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="counter-box mb30-xs">
                            <div class="single-counter">
                                <a href="#" class="pull-left">
                                    <img src="img/3.png" >
                                </a>
                                <div class="media-body">
                                    <h2 class="about-counter" data-num="3498">3498</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="counter-box mb30-xs">
                            <div class="single-counter">
                                <a href="#" class="pull-left">
                                    <img src="img/2.png" >
                                </a>
                                <div class="media-body">
                                    <h2 class="about-counter" data-num="3498">3498</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="counter-box mb30-xs">
                            <div class="single-counter">
                                <a href="#" class="pull-left">
                                    <img src="img/1.png" >
                                </a>
                                <div class="media-body">
                                    <h2 class="about-counter" data-num="3498">3498</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="counter-box mb30-xs">
                            <div class="single-counter">
                                
                                <div class="media-body">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         Counter section End Here -->


        <section id="counter-stats" >
    <div class="container">
        <div class="row">

            <div class="col-lg-3 stats">
                <i class="fa fa-code" aria-hidden="true"></i>
                <div class="counting" data-count="900000">0</div>
                <h5 style="color: #ffffff;">سطر برمجي
</h5>
            </div>

            <div class="col-lg-3 stats">
                <i class="fa fa-check" aria-hidden="true"></i>
                <div class="counting" data-count="280">0</div>
                <h5  style="color: #ffffff;">عمل مُنجز</h5>
            </div>

            <div class="col-lg-3 stats">
                <i class="fa fa-user" aria-hidden="true"></i>
                <div class="counting" data-count="75">0</div>
                <h5  style="color: #ffffff;">عملاء سعداء
</h5>
            </div>

            <div class="col-lg-3 stats">
                <i class="fa fa-coffee" aria-hidden="true"></i>
                <div class="counting" data-count="999">0</div>
                <h5  style="color: #ffffff;">مكالمة عمل</h5>
            </div>


        </div>
        <!-- end row -->
    </div>
    <!-- end container -->

</section>

<!-- end cont stats -->
        <br>
        <br>
 <div class="container">
                    <div class="title-section">
                    <h2 style="color: #ac0101;">هم صفوة عملائنا
 </h2>
                    <h4 style="color: #555555;">سعدنا بالعمل معهم
</h4>
                </div>
    <div class="row">
        <div class="col-md-12">
                <div id="Carousel" class="carousel slide">
                 
                <ol class="carousel-indicators">
                    <li data-target="#Carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#Carousel" data-slide-to="1"></li>
                    <li data-target="#Carousel" data-slide-to="2"></li>
                </ol>
                 
                <!-- Carousel items -->
                <div class="carousel-inner">
                    
                <div class="item active">
                    <div class="row">
                      <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/09.png" alt="Image" style="max-width:100%;"></a></div>
                      <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/10.png" alt="Image" style="max-width:100%;"></a></div>
                      <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/11.png" alt="Image" style="max-width:100%;"></a></div>
                      <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/12.png" alt="Image" style="max-width:100%;"></a></div>
                    </div><!--.row-->
                </div><!--.item-->
                 
                <div class="item">
                    <div class="row">
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/13.png" alt="Image" style="max-width:100%;"></a></div>
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/07.png" alt="Image" style="max-width:100%;"></a></div>
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/08.png" alt="Image" style="max-width:100%;"></a></div>
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/09.png" alt="Image" style="max-width:100%;"></a></div>
                    </div><!--.row-->
                </div><!--.item-->
                 
                <div class="item">
                    <div class="row">
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/09.png" alt="Image" style="max-width:100%;"></a></div>
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/10.png" alt="Image" style="max-width:100%;"></a></div>
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/11.png" alt="Image" style="max-width:100%;"></a></div>
                        <div class="col-md-3"><a href="#" class="thumbnail"><img src="<?php echo base_url() ?>assets/img/client/12.png" alt="Image" style="max-width:100%;"></a></div>
                    </div><!--.row-->
                </div><!--.item-->
                 
                </div><!--.carousel-inner-->
                  <a data-slide="prev" href="#Carousel" class="left carousel-control">‹</a>
                  <a data-slide="next" href="#Carousel" class="right carousel-control">›</a>
                </div><!--.Carousel-->
                 
        </div>
    </div>
</div><!--.container-->


<br>
<br>


 



<!-- Contact page Start Here -->
        <div id="contact" class="contact-page-area padding-top-bottom">
            <div class="container">
                <div class="row">
                    <!-- Main body Start Here -->
                    <div class="body-content">
                        <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                            <form class="contact-form2" id='contact-form' role="form">
                                <fieldset>
                                    <!-- Form Name -->
                                    <legend style="color: #ac0101;">اتصل بنا</legend>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <input id="form-name" name="name" placeholder="الإسم" class="form-control" type="text" data-error="Name field is required" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <input id="form-email" name="email" placeholder="بريدك الإلكتروني" class="form-control" type="text" data-error="E-mail field is required" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <input id="form-subject" name="subject" placeholder="سبب الإتصال" class="form-control" type="text" data-error="Subject field is required" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <!-- Text input-->
                                            <div class="form-group">
                                                <input id="form-phone" name="phone" placeholder="رقم الهاتف" class="form-control" type="text" data-error="Phone field is required" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <!-- Textarea -->
                                            <div class="form-group">
                                                <textarea class="textarea form-control" rows="4" id="form-message" name="message" placeholder="نصّ الرسالة" data-error="Message field is required" required></textarea>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <!-- Button -->
                                            <div class="form-group button-group">
                                                <button type="submit" class="btn-send submit-botton disabled" style="background-color: #ac0101;">أرسل الآن</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='form-response'></div>
                                </fieldset>
                            </form>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                          
                            <div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Al%20Rabwah%20District%20Riyadh&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{position:relative;text-align:right;height:326px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:326px;width:100%;}</style></div>
                        </div>
                    </div>
                    <!-- Main body End Here -->
                </div>
            </div>
        </div>
        <!-- Contact page End Here -->



               

        <!-- Footer section Start Here -->
        <div class="footer-section-area padding-top-bottom">
            <div class="container">
                <div class="row">
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            
                            <img src="<?php echo base_url() ?>assets/img/softifiblanc.png">
                        </div>
                        <div class="information">
<p style="color: white;">مؤسسة سفتيفي تعد إحدى الشركات الرائدة في مجال تطوير البرمجيات وتطبيقات الويب والمواقع الإلكترونية، فنحن ننتج حلول تقنية لقطاعات أعمال مختلفة تتميز بواجهة مهنية مرنة وسهلة الاستخدام</p>
                            
                        </div>
                        </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>خدماتنا </h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">المتاجر الالكترونية</a></li>
                                <li><a href="#">تطبيقات الجوال</a></li>
                                <li><a href="#">تصميم المواقع</a></li>
                                <li><a href="#">تحسين محركات البحث</a></li>
                                <li><a href="#">التسويق الالكتروني</a></li>
                                <li><a href="#">الاستضافة</a></li>
                            </ul>
                        </div>
                    </div>
                          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>روابط هامة
                        </h3>
                        </div>
                        <div class="our-service">
                            <ul>
                                <li><a href="#">                                    الرئيسية
</a></li>
                                <li><a href="#">                                    من نحن
</a></li>
                                <li><a href="#">                                    خدماتنا
</a></li>
                                <li><a href="#">                                      أطلب السعر
</a></li>
                                
                            </ul>
                        </div>
                    </div>
                    

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="title">
                            <h3>القائمة البريدية</h3>
                            <p style="color: #ffffff">تابع الأخبار و العروض أولا بأول
</p>

                        </div>
                        <div class="get-quote">
                            <form id="getQuoteForm" role="form">
                                <fieldset>
                                   
                                    <div class="form-group">
                                        <input id="quote-email" name="email" placeholder="البريد الالكتروني " class="form-control" type="text" data-error="الرجاء إدخال بريدك الالكتروني " required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    
                                    <div class="form-group send-button">
                                        <button type="submit" class="ghost-btn btn-send">إشترك الآن</button>
                                    </div>
                                    <div class="form-response"></div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                                        
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer section End Here -->
        <!-- Copyright section Start Here -->
        <div class="copy-right-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="social-media">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="copy-right">
                            <p>كل الحقوق محفوظة مؤسسة سفتيفي ©2020

</a></p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Copyright section End Here -->
    </div>
    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <!-- jquery-->
    <script src="<?php echo base_url() ?>assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <!-- Plugins js -->
    <script src="<?php echo base_url() ?>assets/js/plugins.js" type="text/javascript"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Meanmenu Js -->
    <script src="<?php echo base_url() ?>assets/js/jquery.meanmenu.min.js" type="text/javascript"></script>
    <!-- Counter Js -->
    <script src="<?php echo base_url() ?>assets/js/jquery.counterup.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/js/waypoints.min.js" type="text/javascript"></script>
    <!-- WOW JS -->
    <script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>
    <!-- Nivo slider js -->
    <script src="<?php echo base_url() ?>assets/vendor/slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/vendor/slider/home.js" type="text/javascript"></script>
    <!-- Owl Cauosel JS -->
    <script src="<?php echo base_url() ?>assets/vendor/OwlCarousel/owl.carousel.min.js" type="text/javascript"></script>
    <!-- Srollup js -->
    <script src="<?php echo base_url() ?>assets/js/jquery.scrollUp.min.js" type="text/javascript"></script>
    <!-- Google Map js -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgREM8KO8hjfbOC0R_btBhQsEQsnpzFGQ"></script>
    <!-- Validator js -->
    <script src="<?php echo base_url() ?>assets/js/validator.min.js" type="text/javascript"></script>
    <!-- One Page menu action JS -->
    <script src="<?php echo base_url() ?>assets/js/jquery.nav.js" type="text/javascript"></script>
    <!-- Custom Js -->
    <script src="<?php echo base_url() ?>assets/js/main.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
    $('#Carousel').carousel({
        interval: 5000
    })
});

        $(document).ready(function(){

    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');
        
        if(value == "all")
        {
            //$('.filter').removeClass('hidden');
            $('.filter').show('1000');
        }
        else
        {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');
            
        }
    });
    
    if ($(".filter-button").removeClass("active")) {
$(this).removeClass("active");
}
$(this).addClass("active");

});

        // number count for stats, using jQuery animate

$('.counting').each(function() {
  var $this = $(this),
      countTo = $this.attr('data-count');
  
  $({ countNum: $this.text()}).animate({
    countNum: countTo
  },

  {

    duration: 3000,
    easing:'linear',
    step: function() {
      $this.text(Math.floor(this.countNum));
    },
    complete: function() {
      $this.text(this.countNum);
      //alert('finished');
    }

  });  
  

});
    </script>
</body>


</html>
