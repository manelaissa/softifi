<div class="page-title">
    <div class="breadcrumb-env">
        <ul class="user-info-menu left-links list-inline list-unstyled">
            <li class="hidden-sm hidden-xs">
                <a href="#" data-toggle="sidebar">
                    <i class="fa-bars"></i>
                </a>
            </li>
        </ul>
        <ol class="breadcrumb bc-1" >
            <li>
                <a href="<?php echo site_url('admin/dashboard') ?>"><i class="fa-home"></i>Home</a>
            </li>
            <li class="active">
                <strong> Slider</strong>
            </li>
        </ol>

    </div>

</div>
<!-- Admin Table-->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Slider</h3>
    </div>
    <div class="panel-body">
        <?php if (validation_errors()) : ?>
            <div class="col-md-12">
                <div class="alert alert-danger">
                    <?php echo validation_errors() ?>
                </div>
            </div>
        <?php endif ?>
        <form role="form" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">

            <div class="form-group">
                <label class="col-sm-2 control-label" for="field-1"> Title Arabic Language</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Title" name="title"
                           value="<?php echo set_value('title', $item->title) ?>">
                </div>
            </div>
            <div class="form-group-separator"></div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Description Arabic Language</label>
                <div class="compose-message-editor col-sm-10">
                    <textarea class="form-control" name="description" placeholder="Description" style="height: 150px;"><?php echo set_value('description', $item->description) ?></textarea>
                </div>
            </div>
            <div class="form-group-separator"></div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="field-1"> Title French Language</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Title" name="title_fr"
                           value="<?php echo set_value('title_fr', $item->title_fr) ?>">
                </div>
            </div>
            <div class="form-group-separator"></div>
<div class="form-group">
                <label class="col-sm-2 control-label">Description French Language</label>
                <div class="compose-message-editor col-sm-10">
                    <textarea class="form-control" name="description_fr" placeholder="Description" style="height: 150px;"><?php echo set_value('description_fr', $item->description_fr) ?></textarea>
                </div>
            </div>
            <div class="form-group-separator"></div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="field-1"> Title English Language</label>

                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Title" name="title_en"
                           value="<?php echo set_value('title_en', $item->title_en) ?>">
                </div>
            </div>
            <div class="form-group-separator"></div>
<div class="form-group">
                <label class="col-sm-2 control-label">Description English Language</label>
                <div class="compose-message-editor col-sm-10">
                    <textarea class="form-control" name="description_en" placeholder="Description" style="height: 150px;"><?php echo set_value('description_en', $item->description_en) ?></textarea>
                </div>
            </div>
            <div class="form-group-separator"></div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label" for="field-1"> Image</label>

                <div class="col-sm-10">
                    <input class="form-control" type="file" name="image" >
                </div>
            </div>
            <div class="form-group-separator"></div>

            <div class="form-group">
                <label class="col-sm-2 control-label"></label>

                <div class="col-sm-10">
                    <input type="submit" class="btn btn-secondary " name="submit" value="Submit">
                    <a href="<?php echo site_url('admin/sliders/index'); ?>" class="btn btn-danger">Cancel</a>
                </div>
            </div>


        </form>

    </div>
</div>

