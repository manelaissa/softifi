<div class="page-title">
    <div class="breadcrumb-env">
        <ul class="user-info-menu left-links list-inline list-unstyled">
            <li class="hidden-sm hidden-xs">
                <a href="#" data-toggle="sidebar">
                    <i class="fa-bars"></i>
                </a>
            </li>
        </ul>
        <ol class="breadcrumb bc-1" >
            <li>
                <a href="<?php echo site_url('admin/dashboard') ?>"><i class="fa-home"></i>Home</a>
            </li>
            <li class="active">
                <strong>About Softifi</strong>
            </li>
        </ol>

    </div>

</div>
<!-- Admin Table-->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> About Softifi</h3>
    </div>
    <div class="panel-body">
        <?php if (validation_errors()) : ?>
            <div class="col-md-12">
                <div class="alert alert-danger">
                    <?php echo validation_errors() ?>
                </div>
            </div>
        <?php endif ?>
        <form role="form" class="form-horizontal" role="form" method="post" enctype="multipart/form-data">

            <div class="form-group">
                <label class="col-sm-2 control-label" for="field-1">About Softifi </label>
                <li><div class="col-sm-10">
                    <textarea type="text" class="form-control" placeholder=" Who we are" name="about"
                              style="height: 200px;"><?php echo set_value('about', $item->about) ?></textarea>
                </div> Arabic Language</li>
                </div>
<div class="form-group">
                <label class="col-sm-2 control-label" for="field-1">About Softifi </label>
 <li><div class="col-sm-10">
                    <textarea type="text" class="form-control" placeholder=" Who we are" name="aboutus_fr"
                              style="height: 200px;"><?php echo set_value('aboutus_fr', $item->aboutus_fr) ?></textarea>
                </div> French Language</li>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="field-1">About Softifi </label>
                 <li><div class="col-sm-10">
                    <textarea type="text" class="form-control" placeholder=" Who we are" name="aboutus_eng"
                              style="height: 200px;"><?php echo set_value('aboutus_eng', $item->aboutus_eng) ?></textarea>
                </div> English Language</li>
               </div> 
                        <div class="form-group-separator"></div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="field-1">Description content  </label>

                <div class="col-sm-10">
                    <textarea type="text" class="form-control" placeholder="Why Choose Us" name="benfits"
                              style="height: 200px;"><?php echo set_value('benfits', $item->benfits) ?></textarea>
                </div>
            </div>
            <div class="form-group-separator"></div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="field-1">Image</label>

                <div class="col-sm-10">
                    <input class="form-control" type="file" name="image" >
                </div>
            </div>
            <div class="form-group-separator"></div>

            
            <div class="form-group-separator"></div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label"></label>

                <div class="col-sm-10">
                    <input type="submit" class="btn btn-secondary " name="submit" value="Submit">
                    <a href="<?php echo site_url('admin/dashboard'); ?>" class="btn btn-danger">Cancel</a>
                </div>
            </div>


        </form>

    </div>
</div>

